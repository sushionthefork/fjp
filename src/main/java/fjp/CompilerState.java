package fjp;

import java.util.*;

class CompilerState {
    // Generated instructions
    static List<String> instructions = new ArrayList<>();

    /**/
    static Stack<TreeListener.JumpSymbols> contextSymbols = new Stack<>();
    static Stack<Integer> jumpAddresses = new Stack<>();
    static Stack<Integer> symbolicAddress = new Stack<>();

    static Stack<List<String>> blockScopes = new Stack<>();

    /**/
    static Stack<List<NumericTermParser>> numericTerms = new Stack<>();
    static Stack<Integer> needReturnValue = new Stack<>();

    // Identifier -> value
    static Map<String, Integer> constants = new HashMap<>();

    // Identifier -> position in stack
    static Map<String, Integer> globalVariables = new HashMap<>();
    static Map<String, Integer> localVariables = new HashMap<>();

    // Identifier -> function
    static Map<String, Function> functions = new HashMap<>();

    static boolean inMain = false;
    static TreeListener.Address ternarIdentifierAddress = null;
    static int ternarJumpInstructionAddress = 0;
    static int ternarTrueJumpInstructionAddress = 0;

    static int logicalTermNestLevel = 0; /* holds the information about nest level of logical terms, allows  a && (b && c)*/
    static int expressionNestLevel = 0; /* holds the information about nest level of expressions, allows a || (b || c) */

    static Stack<TreeListener.Switch> switches = new Stack<>();
}