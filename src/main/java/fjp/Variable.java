package fjp;

class Variable {
    private boolean isValid;
    private boolean isGlobal;
    private int stackPosition;

    Variable(boolean isValid) {
        this.isValid = isValid;
    }

    Variable(boolean isGlobal, int stackPosition) {
        this.isGlobal = isGlobal;
        this.stackPosition = stackPosition;
        this.isValid = true;
    }

    boolean isValid() {
        return isValid;
    }

    boolean isGlobal() {
        return isGlobal;
    }

    int getStackPosition() {
        return stackPosition;
    }
}
