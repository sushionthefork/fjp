package fjp;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import parser.FjpBaseListener;
import parser.FjpParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TreeListener extends FjpBaseListener {
    static final int FALSE = 0;

    private final int MIN_INT = 3; // Necessary space for functions call
    private final int RETURN_STACK_SIZE = 1; // Size on stack for return values

    private final static Logger log = Logger.getLogger(TreeListener.class.getName());

    TreeListener() {
        log.setLevel(Level.FINEST);
    }

    /**
     * Creates jump instruction to main which will be changed later
     * @param ctx context
     */
    @Override
    public void enterTranslation_unit(FjpParser.Translation_unitContext ctx) {
        CompilerState.instructions.add(Instructions.createJmp("@MAIN"));
    }

    /**
     * Prints instructions to stdout
     * @param ctx context
     */
    @Override
    public void exitTranslation_unit(FjpParser.Translation_unitContext ctx) {
        printInstructions();
    }


    /**
     * Sets correct initial jump address (to main function).
     * Creates initial necessary space on stack.
     * Creates space on stack for global variables
     * @param ctx context
     */
    @Override
    public void enterMain_function(FjpParser.Main_functionContext ctx) {
        CompilerState.instructions.set(0, Instructions.createJmp(CompilerState.instructions.size()));
        CompilerState.instructions.add(Instructions.createInt(MIN_INT));
        if (CompilerState.globalVariables.size() > 0)
            CompilerState.instructions.add(Instructions.createInt(CompilerState.globalVariables.size()));
        CompilerState.inMain = true;
        CompilerState.localVariables.clear();
    }

    /**
     * Creates last return instruction
     * @param ctx context
     */
    @Override
    public void exitMain_function(FjpParser.Main_functionContext ctx) {
        CompilerState.instructions.add(Instructions.createRet());
    }

    /**
     * Parses one line of constants.
     * @param ctx context
     */
    @Override
    public void exitConstant(FjpParser.ConstantContext ctx) {
        List<TerminalNode> identifiers = ctx.IDENTIFIER();
        List<TerminalNode> values = ctx.NUMBER();
        String identifier;
        int value;

        log.finest(ctx.getText());

        for (int i = 0; i < identifiers.size(); i++) {
            identifier = identifiers.get(i).getText();
            value = Integer.parseInt(values.get(i).getText());
            log.info("Constant = [identifier:" + identifier + ", value " + value + "]");
            if (CompilerState.globalVariables.containsKey(identifier) || CompilerState.constants.containsKey(identifier)) {
                System.out.println("constant " + identifier + " is already defined.");
                System.exit(1);
            }
            CompilerState.constants.put(identifier, value);
        }
    }

    /**
     * Adds global variables to the list
     * @param ctx context
     */
    @Override
    public void exitGlobal_variables(FjpParser.Global_variablesContext ctx) {
        String identifier;
        int stackAddress;

        log.finest(ctx.getText());
        // Iterate over lines defining global variables
        for (int i = 0; i < ctx.getChildCount(); i++) {
            List<TerminalNode> lineOfIdentifiers = ((FjpParser.VariableContext)ctx.getChild(i)).IDENTIFIER();

            // Iterate over identifiers (from one line)
            for (TerminalNode t : lineOfIdentifiers) {
                identifier = t.getText();
                if (CompilerState.globalVariables.containsKey(identifier) || CompilerState.constants.containsKey(identifier)) {
                    System.out.println("variable " + identifier + " is already defined.");
                    System.exit(1);
                }
                stackAddress = MIN_INT + CompilerState.globalVariables.size();
                CompilerState.globalVariables.put(identifier, stackAddress);
            }
        }
    }

    /**
     * Adds local variables.
     * In case of function local variables -> creates necessary space for function and space for local variables on stack
     * In case of block variables -> crates space for block variables on stack
     * @param ctx context
     */
    @Override
    public void exitLocal_variables(FjpParser.Local_variablesContext ctx) {
        String identifier;
        int stackAddress;

        log.finest(ctx.getText());
        // Iterate over all lines defining local variables
        for (int i = 0; i < ctx.getChildCount(); i++) {
            List<TerminalNode> lineOfIdentifiers = ((FjpParser.VariableContext)ctx.getChild(i)).IDENTIFIER();

            // Iterate over identifiers (from one line)
            for (TerminalNode t : lineOfIdentifiers) {
                identifier = t.getText();
                if (CompilerState.localVariables.containsKey(identifier)) {
                    log.info("Identifier " + identifier + " is already taken.");
                    System.exit(1);
                }
                if (CompilerState.inMain) {
                    stackAddress = MIN_INT + CompilerState.localVariables.size() + CompilerState.globalVariables.size();
                }
                else {
                    stackAddress = MIN_INT + CompilerState.localVariables.size();
                }

                CompilerState.localVariables.put(identifier, stackAddress);
                CompilerState.blockScopes.peek().add(identifier);
            }
        }
        // Create space for function (necessary space for function and local variables)
        if (ctx.getParent().getParent() instanceof FjpParser.FunctionContext) {
            String funcIdentifier = ((FjpParser.FunctionContext)ctx.getParent().getParent()).function_header().IDENTIFIER().getText();
            Function function = CompilerState.functions.get(funcIdentifier);
            int localVariablesCount = CompilerState.localVariables.size() - function.getNumOfParameters();
            CompilerState.instructions.add(Instructions.createInt(MIN_INT + localVariablesCount));
        }
        // Block variables
        else {
            if (CompilerState.blockScopes.peek().size() > 0) {
                CompilerState.instructions.add(Instructions.createInt(CompilerState.blockScopes.peek().size()));
            }
        }
    }

    /**
     * Clears local variables
     * @param ctx context
     */
    @Override
    public void enterFunction(FjpParser.FunctionContext ctx) {
        log.finest(ctx.getText());
        log.info("Clearing local variables");
        CompilerState.localVariables.clear();
    }

    /**
     * Adds function to the list
     * @param ctx context
     */
    @Override
    public void exitFunction_header(FjpParser.Function_headerContext ctx) {
        String funcIdentifier = ctx.IDENTIFIER().getText();
        if (CompilerState.functions.containsKey(funcIdentifier)) {
            log.info("fjp.Function '" + funcIdentifier + "' already defined.");
            System.out.println("fjp.Function '" + funcIdentifier + "' already defined.");
            System.exit(1);
        }
        else {
            int firstInstruction = CompilerState.instructions.size();
            int numOfParameters = ctx.function_parameters().function_parameter().size();
            boolean shouldReturnValue = (ctx.VOID() == null);

            log.info("fjp.Function '" + funcIdentifier + "' defined " + "[first instruction: " + firstInstruction + ", parameters: " + numOfParameters + ", returns value: " + shouldReturnValue + "]");
            CompilerState.functions.put(funcIdentifier, new Function(firstInstruction, numOfParameters, shouldReturnValue));

            // Parameters
            String identifier;
            for (int i = 0; i < numOfParameters; i++) {
                identifier = ctx.function_parameters().function_parameter(i).IDENTIFIER().getText();

                if (CompilerState.localVariables.containsKey(identifier)) {
                    log.info("Duplicate declaration of parameter '" + identifier + "'.");
                    System.exit(1);
                }

                CompilerState.localVariables.put(identifier, -(numOfParameters - i));
            }
        }
    }

    /**
     * Adds instruction for storage of the return value
     * @param ctx context
     */
    @Override
    public void exitFunction_return(FjpParser.Function_returnContext ctx) {
        String funcIdentifier = ((FjpParser.FunctionContext)ctx.getParent()).function_header().IDENTIFIER().getText();
        Function function = CompilerState.functions.get(funcIdentifier);
        function.setReturnsValue(true);
        CompilerState.instructions.add(
                Instructions.createSto(0, -(function.getNumOfParameters() + 1))
        );
    }

    /**
     * Creates return instruction at the end of the function (and checks return constistency)
     * @param ctx context
     */
    @Override
    public void exitFunction(FjpParser.FunctionContext ctx) {
        String funcIdentifier = ctx.function_header().IDENTIFIER().getText();
        if (!CompilerState.functions.get(funcIdentifier).isConsistent()) {
            log.info("fjp.Function '" + funcIdentifier + "' is inconsistent in return value in header and body.");
            System.out.println("fjp.Function '" + funcIdentifier + "' is inconsistent in return value in header and body.");
            System.exit(1);
        }

        CompilerState.instructions.add(Instructions.createRet());
    }

    /**
     * Allocates space for return value on stack
     * @param ctx context
     */
    @Override
    public void exitFunction_identifier(FjpParser.Function_identifierContext ctx) {
        String identifier = ctx.IDENTIFIER().getText();
        if (CompilerState.functions.containsKey(identifier)) {
            Function function = CompilerState.functions.get(identifier);
            if (function.doesReturnValue()) {
                CompilerState.needReturnValue.push(1);
            }
        }
        else {
            log.warning("fjp.Function '" + identifier + "' is not defined.");
            System.out.println("fjp.Function '" + identifier + "' is not defined.");
            System.exit(1);
        }
    }

    /**
     * Creates space on stack for return value.
     * In case of plain function call (not part of an expression) -> crate call instruction and then pop parameters and
     * space for return value from stack.
     * @param ctx context
     */
    @Override
    public void exitFunction_call(FjpParser.Function_callContext ctx) {
        if (!CompilerState.needReturnValue.empty()) { // parameterless function, expression didn't pop
            CompilerState.instructions.add(Instructions.createInt(RETURN_STACK_SIZE));
        }

        // Handle here only plain function call (not part of an expression)
        if (ctx.getParent() instanceof FjpParser.CommandContext) {
            Function function = Function.checkFunctionCall(ctx, CompilerState.functions);

            CompilerState.instructions.add(Instructions.createCal(function.getFirstInstruction()));

            // Pop parameters and return value from stack
            int toPop = -(function.getNumOfParameters() + (function.doesReturnValue() ? RETURN_STACK_SIZE : 0));
            if (toPop != 0) {
                CompilerState.instructions.add(Instructions.createInt(toPop));
            }
        }
    }

    /**
     * Creates new block scope for block variables
     * @param ctx context
     */
    @Override
    public void enterBlock_body(FjpParser.Block_bodyContext ctx) {
        CompilerState.blockScopes.add(new ArrayList<>());
    }


    /**
     * Removing block scope and all it's variables from available local variables
     * @param ctx context
     */
    @Override
    public void exitBlock_body(FjpParser.Block_bodyContext ctx) {
        List<String> blockScope = CompilerState.blockScopes.pop();
        if (((blockScope.size() > 0)
                && !(ctx.getParent() instanceof FjpParser.FunctionContext)
                && !(ctx.getParent() instanceof FjpParser.Main_functionContext))) {
            CompilerState.instructions.add(Instructions.createInt(-(blockScope.size())));

            for (String variable : blockScope) {
                CompilerState.localVariables.remove(variable);
            }
        }
    }


    /**
     * Entering jump address to jump back to at the end of do while in case expression is true
     * @param ctx context
     */
    @Override
    public void enterDo_while(FjpParser.Do_whileContext ctx) {
        CompilerState.jumpAddresses.add(CompilerState.instructions.size());
    }


    /**
     * Jumping based on the expression behind the do while cycle, we are negating the value of expression for JMC
     * @param ctx context
     */
    @Override
    public void exitDo_while(FjpParser.Do_whileContext ctx) {
        CompilerState.instructions.add(Instructions.createLit(FALSE)); // negating expression, jmc jumps if top of the stack is zero
        CompilerState.instructions.add(Instructions.createEq());
        CompilerState.instructions.add(Instructions.createJmc(CompilerState.jumpAddresses.pop()));
    }

    /**
     * Adding context symbol that we want to jump based on the expression and at the end of the loop we want to jump before it
     * @param ctx context
     */
    @Override
    public void enterWhile_cycle(FjpParser.While_cycleContext ctx) {
        CompilerState.contextSymbols.add(JumpSymbols.EXPRESSION_LOOP_JUMP);
    }


    /**
     * Adding jump before the evaluatin of the condition expression and setting correct JMC after condition expression
     * to jump behind the while body
     * @param ctx context
     */
    @Override
    public void exitWhile_cycle(FjpParser.While_cycleContext ctx) {
        log.finest(ctx.getText());

        if (!CompilerState.jumpAddresses.empty()) {
            CompilerState.instructions.add(Instructions.createJmp(CompilerState.jumpAddresses.pop()));
            CompilerState.instructions.set(CompilerState.symbolicAddress.pop(), Instructions.createJmc(CompilerState.instructions.size()));
        }
    }

    /**
     * Adding expression jump to the symbol stack, so we know that we want to jump based on the expression
     * @param ctx context
     */
    @Override
    public void enterIf_clause(FjpParser.If_clauseContext ctx) {
        CompilerState.contextSymbols.add(JumpSymbols.EXPRESSION_IF_JUMP); // jumping based on expression
    }

    /**
     * If clause is part of the if_else clause then the method doesn't do anything since everything important is done in the enterElse_clause method
     * if it is not, then a address with symbolic jump behind the if body is popped and replaced with a jump to correct address
     * @param ctx context
     */
    @Override
    public void exitIf_clause(FjpParser.If_clauseContext ctx) {
        // exiting if which is part of if_else, we don't do anything
        // jumping from the end of if clause to behind else clause is done in enterElse_Clause
        // jumping from after evaluation of expression to beginning of else is also done in enterClause
        if (!(ctx.getParent() instanceof FjpParser.If_else_clauseContext) && !CompilerState.symbolicAddress.empty()) {
            int foo = CompilerState.symbolicAddress.pop();
            CompilerState.instructions.set(foo, Instructions.createJmc(CompilerState.instructions.size()));
            // the end of if clause, we have to replace the symbolic jump behind the evaluation of the expression to jump behind the if body
        }
        else {
            log.warning("Unsuccessful exit if clause, didn't find else_jump token nor symbolic address to change");
        }
    }


    /**
     * Adding first instruction as a jump from the end of the if_clause body to behind of else_clause body
     * Setting correct JMC after condition expression in if_clause to jump to first instruction of else_clause body
     * Adding index of symbolic jump to behind of the else_clause body
     * @param ctx context
     */
    @Override
    public void enterElse_clause(FjpParser.Else_clauseContext ctx) {
        CompilerState.instructions.add(Instructions.createJmp("@SYMBOL")); // jumping from the end of if clause to the beginning right behind else clause
        if (!CompilerState.symbolicAddress.empty()) {
            CompilerState.instructions.set(CompilerState.symbolicAddress.pop(), Instructions.createJmc(CompilerState.instructions.size())); // replacing jmc after expression in if clause
        }
        else {
            log.warning("No symbolic address in stack");
        }
        CompilerState.symbolicAddress.add(CompilerState.instructions.size() - 1); // adding instruction to be changed
    }


    /**
     * Setting correct JMP behind the else_clause body
     * @param ctx context
     */
    @Override
    public void exitElse_clause(FjpParser.Else_clauseContext ctx) {
        if (!CompilerState.symbolicAddress.empty()) {
            CompilerState.instructions.set(CompilerState.symbolicAddress.pop(), Instructions.createJmp(CompilerState.instructions.size()));
        }
        else {
            log.warning("No symbolic address in stack");
        }
    }

    /**
     * If top of the symbol stack is @EXPRESSION_LOOP_JUMP then we add a instruction index into jumpAddresses, so we can jump right before
     * evaluation of the expression usually popped by while_cycle
     * @param ctx context
     */
    @Override
    public void enterExpression(FjpParser.ExpressionContext ctx) {
        log.finest(ctx.getText());

        if (!CompilerState.contextSymbols.empty() && CompilerState.contextSymbols.peek().equals(JumpSymbols.EXPRESSION_LOOP_JUMP)) {
            CompilerState.jumpAddresses.add(CompilerState.instructions.size());
        }
        if (!CompilerState.needReturnValue.empty()) { // increase stack before first expression
            CompilerState.needReturnValue.pop();
            CompilerState.instructions.add(Instructions.createInt(RETURN_STACK_SIZE));
        }

        CompilerState.expressionNestLevel++;
    }

    /**
     * First we create postfix notation so we can properly use PL0 instruction set
     * If expression_* symbol is on top of the stack then we include a symbolic JMC based on the expression
     * which later has to be set upon exiting if, while, do_while
     * @param ctx context
     */
    @Override
    public void exitExpression(FjpParser.ExpressionContext ctx) {
        List<Integer> positions = new ArrayList<>();
        int index = 0;
        int instructionsAdded = 0;
        final int OR_TRUE = 1;

        log.finest(ctx.getText());

        Iterator<String> it = CompilerState.instructions.iterator();
        while (it.hasNext()) {
            if (it.next().equals(InstructionSymbols.LOGICAL_TERM_END.getSymbol() + CompilerState.expressionNestLevel)) {
                positions.add(index);
                index--;
                it.remove();
            }
            index++;
        }

        /* postfix notation, two operands on stack, then we add one operand */
        if (positions.size() > 1) {
            for (int i = 0; i < ctx.OR().size() + 1; i++) {
                CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createLit(FALSE));
                CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createNeq()); // comparing whatever was on stack with false, if different then 1 on stack
                log.info("OR");
                if (i > 0) { // two operands on stack before we do anything
                    CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createAdd());
                    CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createLit(OR_TRUE)); // if they add up to 2, then comparison && comparison evaluate to true
                    CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createGe());
                }
            }
        }

        /* Checking for expression* symbol on stack, if set then we put symbolic JMC and pop the symbol */
        if (!CompilerState.contextSymbols.empty() &&
                (CompilerState.contextSymbols.peek().equals(JumpSymbols.EXPRESSION_LOOP_JUMP) ||
                CompilerState.contextSymbols.peek().equals(JumpSymbols.EXPRESSION_IF_JUMP))) {
            CompilerState.instructions.add(Instructions.createJmc("@SYMBOL"));
            CompilerState.symbolicAddress.add(CompilerState.instructions.size() - 1); // position of instruction with @symbol
            CompilerState.contextSymbols.pop();

            log.info("Adding symbolic instruction on " + CompilerState.instructions.size() + ". position");
        }

        log.info("Expression '" + ctx.getText() + "' successfully evaluated.");
        CompilerState.expressionNestLevel--;
    }

    /**
     * Increase nesting level of logical terms
     * @param ctx context
     */
    @Override
    public void enterLogical_term(FjpParser.Logical_termContext ctx) {
        CompilerState.logicalTermNestLevel++;
    }

    /**
     * Creating postfix notation and then evaluating terms consisting of && operands, we turn every comparison unit
     * to either 0 or 1 using '0' and != (NotEqual) operator which yields 0 for 0 and 1 for everything else.
     * Puts behind a symbolic instruction signaling the end of logical term.
     * @param ctx context
     */
    @Override
    public void exitLogical_term(FjpParser.Logical_termContext ctx) {
        List<Integer> positions = new ArrayList<>();
        int index = 0;
        int instructionsAdded = 0;
        final int AND_TRUE = 2;
        log.finest(ctx.getText());

        Iterator<String> it = CompilerState.instructions.iterator();
        while (it.hasNext()) {
            if (it.next().equals(InstructionSymbols.COMPARISON_END.getSymbol() + CompilerState.logicalTermNestLevel)) {
                positions.add(index);
                index--;
                it.remove();
            }
            index++;
        }

        if (positions.size() > 1) {
            for (int i = 0; i < ctx.AND().size() + 1; i++) {
                CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createLit(FALSE));
                CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createNeq()); // comparing whatever was on stack with false, if different then 1 on stack
                log.info("AND");
                if (i > 0) {
                    CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createAdd()); // adding up two top values on stack
                    CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createLit(AND_TRUE)); // if they add up to 2, then comparison && comparison evaluate to true
                    CompilerState.instructions.add(positions.get(i) + instructionsAdded++, Instructions.createEq());
                }
            }
        }

        CompilerState.instructions.add(InstructionSymbols.LOGICAL_TERM_END.getSymbol() + CompilerState.expressionNestLevel);
        CompilerState.logicalTermNestLevel--;
    }

    /**
     * Inserts correct operator into instructions and puts a symbolic instruction behind
     * signaling the end of the comparison unit.
     * @param ctx context
     */
    @Override
    public void exitComparison(FjpParser.ComparisonContext ctx) {
        log.finest(ctx.getText());

        for (int i = 0; i < ctx.getChildCount(); i++) {
            if (ctx.getChild(i) instanceof TerminalNode) {
                switch (((TerminalNode) ctx.getChild(i)).getSymbol().getType()) {
                    case FjpParser.EQ:
                        CompilerState.instructions.add(Instructions.createEq());
                        log.info("Adding instruction: EQ");
                        break;
                    case FjpParser.NEQ:
                        CompilerState.instructions.add(Instructions.createNeq());
                        log.info("Adding instruction: NEQ");
                        break;
                    case FjpParser.LT:
                        CompilerState.instructions.add(Instructions.createLt());
                        log.info("Adding instruction: LT");
                        break;
                    case FjpParser.LE:
                        CompilerState.instructions.add(Instructions.createLe());
                        log.info("Adding instruction: LE");
                        break;
                    case FjpParser.GT:
                        CompilerState.instructions.add(Instructions.createGt());
                        log.info("Adding instruction: GT");
                        break;
                    case FjpParser.GE:
                        CompilerState.instructions.add(Instructions.createGe());
                        log.info("Adding instruction: GE");
                        break;
                }
            }
        }
        CompilerState.instructions.add(InstructionSymbols.COMPARISON_END.getSymbol() + CompilerState.logicalTermNestLevel);
    }

    /**
     * Only clears all numeric terms, might be redundant because it's done at the end of numeric expression
     * @param ctx context
     */
    @Override
    public void enterNumeric_expression(FjpParser.Numeric_expressionContext ctx) {
        log.finest(ctx.getText());
        log.info("Clearing numeric terms");
        CompilerState.numericTerms.add(new ArrayList<>());
    }

    @Override
    public void enterNumeric_term(FjpParser.Numeric_termContext ctx) {
        CompilerState.numericTerms.peek().add(new NumericTermParser());
    }

    /**
     * Uses an array of numeric_terms and inserts addition and subtraction using postfix notation
     * @param ctx context
     */
    @Override
    public void exitNumeric_expression(FjpParser.Numeric_expressionContext ctx) {
        int numericTermsIndex = 0;
        int addSubInstrIndex = 0;
        List<String> addSubInstructions = new ArrayList<>();

        log.finest(ctx.getText());

        for (int i = 0; i < ctx.getChildCount(); i++) {
            if (ctx.getChild(i) instanceof TerminalNode) {
                switch (((TerminalNode) ctx.getChild(i)).getSymbol().getType()) {
                    case FjpParser.ADD:
                        addSubInstructions.add(Instructions.createAdd());
                        log.info("Adding instruction: ADD");
                        break;
                    case FjpParser.SUB:
                        if (i == 0) {
                            addSubInstructions.add(Instructions.createUnaryMinus());
                            log.info("Adding instruction: UNARY SUB");
                        }
                        else {
                            addSubInstructions.add(Instructions.createSub());
                            log.info("Adding instruction: SUB");
                        }
                        break;
                }
            }
        }

        while (numericTermsIndex < CompilerState.numericTerms.peek().size()) {
            CompilerState.instructions.addAll(CompilerState.numericTerms.peek().get(numericTermsIndex++).getInstructions());
            if (addSubInstrIndex < addSubInstructions.size()) {
                if (addSubInstructions.get(addSubInstrIndex).equals(Instructions.createUnaryMinus())) {
                    CompilerState.instructions.add(addSubInstructions.get(addSubInstrIndex++));
                    continue;
                }
            }

            if (numericTermsIndex == 1) { /* we need to have two operands on stack before putting operator */
                continue;
            }
            if (addSubInstrIndex < addSubInstructions.size()) {
                CompilerState.instructions.add(addSubInstructions.get(addSubInstrIndex++));
            }
        }
        CompilerState.numericTerms.pop();
    }

    /**
     * Loads operands and operators into NumericTerm parser
     * @param ctx context
     */
    @Override
    public void exitNumeric_term(FjpParser.Numeric_termContext ctx) {
        log.info(ctx.getText());
        List<ParseTree> operators = new ArrayList<>();
        List<ParseTree> operands = new ArrayList<>();

        for (int i = 0; i < ctx.getChildCount(); i++) {
            ParseTree child = ctx.getChild(i);
            if (child instanceof TerminalNode) {
                operators.add(child);
            }
            else {
                try {
                    if (((TerminalNode) child.getChild(0)).getSymbol().getType() == FjpParser.NEG) {
                        operands.add(child.getChild(1).getChild(0));
                        operands.add(child.getChild(0));
                    }
                    else {
                        operands.add(child.getChild(0));
                    }
                } catch (ClassCastException cce) {
                    operands.add(child.getChild(0));

                }
            }
        }
        NumericTermParser numericTerm = CompilerState.numericTerms.peek().get(CompilerState.numericTerms.peek().size() - 1);
        numericTerm.setTerm(operators, operands);
        numericTerm.parse();
    }

    /**
     * Regular and multiple assignment
     */
    @Override
    public void exitAssignment(FjpParser.AssignmentContext ctx) {
        log.finest(ctx.getText());
        List<TerminalNode> identifiers = ctx.IDENTIFIER();

        String identifier;
        Address address;
        for (int i = 0; i < identifiers.size(); i++) {
            identifier = identifiers.get(i).getText();
            address = getVariableAddress(identifier);
            if (address != null) {
                if (CompilerState.inMain) {
                    address.level = 0;
                }
                CompilerState.instructions.add(Instructions.createSto(address.level, address.offset));
                // Put value on the stack for the next assignment
                if (i < identifiers.size() - 1) {
                    CompilerState.instructions.add(Instructions.createLod(address.level, address.offset));
                }
            }
            else {
                log.warning("Assignment '" + ctx.getText() + "' failed.");
                System.out.println("fjp.Variable '" + identifier + "' is not declared/defined.");
                System.exit(1);
            }
        }

        log.info("Successful assignment '" + ctx.getText() + "'.");
    }

    /**
     * Handles parallel assignment
     * @param ctx context
     */
    @Override
    public void exitParallel_assignment(FjpParser.Parallel_assignmentContext ctx) {
        List<TerminalNode> identifiers = ctx.IDENTIFIER();
        List<FjpParser.ExpressionContext> expressions = ctx.expression();

        log.finest(ctx.getText());

        if (identifiers.size() != expressions.size()) {
            log.info("Parallel assignment '" + ctx.getText() + "' failed (identifiers.size != expressions.size)");
            System.out.println("Number of identifiers is not equal to the number of expressions.");
            System.exit(1);
        }

        String identifier;
        Address address;
        for (int i = identifiers.size() - 1; i >= 0; i--) {
            identifier = identifiers.get(i).getText();
            address = getVariableAddress(identifier);
            if (address != null) {
                CompilerState.instructions.add(Instructions.createSto(address.level, address.offset));
            }
            else {
                log.info("Assignment '" + ctx.getText() + "' failed.");
                System.out.println("fjp.Variable '" + identifier + "' is not defined.");
                System.exit(1);
            }
        }
    }

    /**
     * Finds address of the variable.
     * @param ctx context
     */
    @Override
    public void exitTernar_identifier(FjpParser.Ternar_identifierContext ctx) {
        String identifier = ctx.getText();
        CompilerState.ternarIdentifierAddress = getVariableAddress(identifier);
        if (CompilerState.ternarIdentifierAddress == null) {
            log.warning("Assignment '" + ctx.getParent().getText() + "' failed.");
            System.out.println("fjp.Variable '" + identifier + "' is not declared/defined.");
            System.exit(1);
        }
    }

    /**
     * Creates instruction for conditional jump to the "false branch" of conditional assignment which will be changed
     * later.
     * @param ctx context
     */
    @Override
    public void exitTernar_condition(FjpParser.Ternar_conditionContext ctx) {
        CompilerState.ternarJumpInstructionAddress = CompilerState.instructions.size();
        CompilerState.instructions.add(Instructions.createJmc(0)); // This instruction will be changed later
    }

    /**
     * Creates "true branch" of the conditional assignment.
     * Creates instruction for the jump at the end of this assignment (skips "false branch") which will be changed later.
     * @param ctx context
     */
    @Override
    public void exitTernar_true(FjpParser.Ternar_trueContext ctx) {
        Address address = CompilerState.ternarIdentifierAddress;
        CompilerState.instructions.add(Instructions.createSto(address.level, address.offset));
        CompilerState.ternarTrueJumpInstructionAddress = CompilerState.instructions.size();
        CompilerState.instructions.add(Instructions.createJmp(0)); // This instruction will be changed later
    }

    /**
     * Modifies instruction for conditional jump to the "false branch" of conditional assignment.
     * @param ctx context
     */
    @Override
    public void enterTernar_false(FjpParser.Ternar_falseContext ctx) {
        CompilerState.instructions.set(CompilerState.ternarJumpInstructionAddress,
                Instructions.createJmc(CompilerState.instructions.size()));
    }

    /**
     * Creates "false branch" of the conditional assignment.
     * Modifies instruction for the jump at the end of this assignment (skips "false branch").
     * @param ctx context
     */
    @Override
    public void exitTernar_false(FjpParser.Ternar_falseContext ctx) {
        Address address = CompilerState.ternarIdentifierAddress;
        CompilerState.instructions.add(Instructions.createSto(address.level, address.offset));
        CompilerState.instructions.set(CompilerState.ternarTrueJumpInstructionAddress,
                Instructions.createJmp(CompilerState.instructions.size()));
    }

    /**
     * Creates new switch
     * @param ctx context
     */
    @Override
    public void enterSwitch_command(FjpParser.Switch_commandContext ctx) {
        CompilerState.switches.push(new Switch());
    }

    /**
     * Creates temporary variable with unique name where value of evaluated switch condition is stored
     * @param ctx context
     */
    @Override
    public void exitSwitch_condition(FjpParser.Switch_conditionContext ctx) {
        final String basicSwitchVarName = "@switch"; // Basic variable name for evaluated switch condition

        String varName = basicSwitchVarName + CompilerState.switches.size();
        CompilerState.switches.peek().evaluatedConditionVariableName = varName;
        CompilerState.localVariables.put(varName, MIN_INT + CompilerState.localVariables.size());
    }
    /**
     * Creates new switch case
     * @param ctx context
     */
    @Override
    public void enterSwitch_case(FjpParser.Switch_caseContext ctx) {
        SwitchCase switchCase = new SwitchCase();
        switchCase.firstInstruction = CompilerState.instructions.size();
        CompilerState.switches.peek().cases.add(switchCase);
    }

    /**
     * Creates instruction for comparison of this case's expression and evaluated switch condition and conditional jump
     * to the next switch case which will be changed later.
     * @param ctx context
     */
    @Override
    public void exitSwitch_case_expression(FjpParser.Switch_case_expressionContext ctx) {
        SwitchCase switchCase = CompilerState.switches.peek().getLastCase();
        Switch sw = CompilerState.switches.peek();
        CompilerState.instructions.add(Instructions.createLod(0, sw.getEvaluatedConditionAddress()));
        CompilerState.instructions.add(Instructions.createEq());

        switchCase.jumpToNextCaseAddress = CompilerState.instructions.size();
        // This instruction will be changed later to a conditional jump to the next switch case)
        CompilerState.instructions.add(Instructions.createJmc(0));
    }

    /**
     * Creates instruction for the jump to the end of this switch (skips other cases). This instruction will be changed
     * later.
     * @param ctx context
     */
    @Override
    public void exitSwitch_case(FjpParser.Switch_caseContext ctx) {
        SwitchCase switchCase = CompilerState.switches.peek().getLastCase();

        switchCase.jumpToEndAddress = CompilerState.instructions.size();
        // This instructions will be changed later to a jump to the end of the switch (skip other switch cases)
        CompilerState.instructions.add(Instructions.createJmc(0));
    }

    /**
     * Sets correct addresses of all case's jumps.
     * Clears temporary variable that holds value of evaluated switch condition.
     * @param ctx context
     */
    @Override
    public void exitSwitch_command(FjpParser.Switch_commandContext ctx) {
        Switch sw = CompilerState.switches.pop();
        int endAddress = CompilerState.instructions.size();

        SwitchCase switchCase;
        for (int i = 0; i < sw.cases.size(); i++) {
            switchCase = sw.cases.get(i);

            if (i == sw.cases.size() - 1) { // Last case -> set address of conditional jump to the end of the switch
                CompilerState.instructions.set(switchCase.jumpToNextCaseAddress,
                        Instructions.createJmc(endAddress));
            } else { // Set address of conditional jump to the next case
                CompilerState.instructions.set(switchCase.jumpToNextCaseAddress,
                        Instructions.createJmc(sw.cases.get(i + 1).firstInstruction));
            }

            CompilerState.instructions.set(switchCase.jumpToEndAddress, Instructions.createJmp(endAddress));
        }

        // Pop evaluated switch condition from the stack and remove it form local variables
        CompilerState.localVariables.remove(sw.evaluatedConditionVariableName);
        CompilerState.instructions.add(Instructions.createInt(-1));
    }


    /* public interface */

    void dumpToFile(String fileName) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
            int i = 0;
            for (String instruction : CompilerState.instructions) {
                bw.write(i++ + " " + instruction);
                bw.newLine();
            }
            bw.close();
        } catch (IOException ioe) {
            System.err.println("Could write into file " + fileName);
            ioe.printStackTrace();
        }
    }


    /* private */
    private void printInstructions() {
        int i = 0;
        for (String instruction : CompilerState.instructions) {
            System.out.println(i++ + " " +  instruction);
        }
    }

    private Address getVariableAddress(String identifier) {
        if (CompilerState.localVariables.containsKey(identifier)) {
            return new Address(0, CompilerState.localVariables.get(identifier));
        }
        else if (CompilerState.globalVariables.containsKey(identifier)) {
            return new Address(1, CompilerState.globalVariables.get(identifier));
        }
        else {
            return null;
        }
    }

    class Address {
        int level;
        int offset;

        Address(int level, int offset) {
            this.level = level;
            this.offset = offset;
        }
    }

    class Switch {
        String evaluatedConditionVariableName; // Variable name where evaluated condition's value is stored
        List<SwitchCase> cases; // Cases of this switch

        Switch() {
            cases = new ArrayList<>();
        }

        SwitchCase getLastCase() {
            return cases.get(cases.size() - 1);
        }

        int getEvaluatedConditionAddress() {
            return CompilerState.localVariables.get(evaluatedConditionVariableName);
        }
    }

    class SwitchCase {
        int firstInstruction; // First instruction of this case
        int jumpToNextCaseAddress; // Address where the conditional jump to the next switch case should be
        int jumpToEndAddress; // Address where the jump to the end of switch should be
    }

    enum InstructionSymbols {
        COMPARISON_END("@COMPARISON_END"),
        LOGICAL_TERM_END("@LOGICAL_TERM_END"),
        ;

        private String symbol;

        InstructionSymbols(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }
    }

    enum JumpSymbols {
        EXPRESSION_LOOP_JUMP("@EXPRESSION_LOOP_JUMP"), // loop jump based on expression, jumping back to evaluating expression
        EXPRESSION_IF_JUMP("@EXPRESSION_IF_JUMP"), // jumping based on if
        ;

        String symbol;

        JumpSymbols(String symbol) {
            this.symbol = symbol;
        }
    }
}