package fjp;

public enum KeyWords {
    TRUE("true"),
    FALSE("false"),
    ;

    private final String keyWord;

    KeyWords(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getKeyWord() {
        return keyWord;
    }
}
