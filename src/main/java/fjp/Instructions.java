package fjp;

import org.antlr.v4.runtime.tree.TerminalNode;
import parser.FjpParser;

/**
 * Class providing generation of instructions for PL0
 */

class Instructions {

    static String createLit(int constant) {
        return "LIT 0, " + constant;
    }

    static String createLod(int level, int offset) {
        return "LOD " + level + ", " + offset;
    }

    static String createSto(int level, int offset) {
        return "STO " + level + ", " + offset;
    }

    static String createCal(int address) {
        int level = CompilerState.inMain ? 0 : 1;
        return "CAL " + level + ", " + address;
    }

    static String createInt(int size) {
        return "INT 0, " + size;
    }

    static String createJmp(String address) {
        return "JMP 0, " + address;
    }

    static String createJmp(int address) {
        return createJmp(address + "");
    }

    static String createJmc(String address) {
        return "JMC 0, " + address;
    }

    static String createJmc(int address) {
        return createJmc(address + "");
    }

    static String createRet() {
        return "RET 0, 0";
    }

    // Operators
    static String createUnaryMinus() {
        return "OPR 0, 1";
    }

    static String createAdd() {
        return "OPR 0, 2";
    }

    static String createSub() {
        return "OPR 0, 3";
    }

    static String createMul() {
        return "OPR 0, 4";
    }

    static String createDiv() {
        return "OPR 0, 5";
    }

    static String createMod() {
        return "OPR 0, 6";
    }

    static String createOdd() {
        return "OPR 0, 7";
    }

    static String createEq() {
        return "OPR 0, 8";
    }

    static String createNeq() {
        return "OPR 0, 9";
    }

    static String createLt() {
        return "OPR 0, 10";
    }

    static String createGe() {
        return "OPR 0, 11";
    }

    static String createGt() {
        return "OPR 0, 12";
    }

    static String createLe() {
        return "OPR 0, 13";
    }

    static String resolveOperator(TerminalNode node) {
        switch (node.getSymbol().getType()) {
            case FjpParser.MUL:
                return createMul();
            case FjpParser.DIV:
                return createDiv();
            default:
                return "not implemented yet";
        }
    }

}