package fjp;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import parser.*;

import java.io.IOException;

public class Main {

    private static final int ARGUMENTS_REQUIRED = 1;

    private static void printHelp() {
        System.out.println("Compiler by hoanghun(at)students.zcu.cz and jirakv(at)students.zcu.cz");
        System.out.println("Usage: java Main <file_name>");
        System.out.println("Outputs <file_name>.pl0 file");
    }

    public static void main(String[] args) {
        if (args.length != ARGUMENTS_REQUIRED) {
            System.out.println("Wrong arguments, use -h flag");
        }
        else {
            if (args[0].equals("-h")) {
                printHelp();
                return;
            }

            try {
                FjpLexer lexer = new FjpLexer(CharStreams.fromFileName(args[0]));
                FjpParser parser = new FjpParser(new CommonTokenStream(lexer));

                TreeListener listener = new TreeListener();

                parser.addParseListener(listener);
                parser.removeErrorListeners();
                parser.addErrorListener(ThrowingErrorListener.INSTANCE);

                parser.translation_unit();

                listener.dumpToFile(args[0].substring(0, args[0].lastIndexOf('.')) + ".pl0");
            } catch (IOException e) {
                System.out.println("Input file not found");
            } catch (ParseCancellationException pce) {
                System.out.println("Parsing failed because: ");
                pce.printStackTrace();
            }
        }
    }
}