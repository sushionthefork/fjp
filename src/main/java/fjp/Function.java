package fjp;

import parser.FjpParser;

import java.util.Map;
import java.util.logging.Logger;

class Function {
    private int numOfParameters;
    private int firstInstruction;
    private boolean shouldReturnValue; // Whether function declares that it returns a value
    private boolean returnsValue = false; // Whether function actually returns value in body

    private final static Logger log = Logger.getLogger(Function.class.getName());

    Function(int firstInstruction, int numOfParameters, boolean shouldReturnValue) {
        this.firstInstruction = firstInstruction;
        this.numOfParameters = numOfParameters;
        this.shouldReturnValue = shouldReturnValue;
    }

    int getFirstInstruction() {
        return firstInstruction;
    }

    boolean isConsistent() {
        return shouldReturnValue == returnsValue;
    }

    void setReturnsValue(boolean returnsValue) {
        this.returnsValue = returnsValue;
    }

    boolean doesReturnValue() {
        return shouldReturnValue;
    }

    int getNumOfParameters() {
        return numOfParameters;
    }

    static Function checkFunctionCall(final FjpParser.Function_callContext ctx, final Map<String, Function> functions) {
        String identifier = ctx.function_identifier().IDENTIFIER().getText();
        if (!functions.containsKey(identifier)) {
            log.warning("fjp.Function '" + identifier + "' is not defined.");
            System.out.println("fjp.Function '" + identifier + "' is not defined.");
            System.exit(1);
        }
        else {
            int numberOfPassedParameters = ctx.expression().size();
            boolean returnValueRequired = !(ctx.getParent() instanceof FjpParser.CommandContext);
            Function function = functions.get(identifier);

            if (function.getNumOfParameters() != numberOfPassedParameters) {
                log.warning("fjp.Function '" + identifier + "' requires " + function.getNumOfParameters() + " parameters. " + numberOfPassedParameters + " parameters passed.");
                System.out.println("fjp.Function '" + identifier + "' requires " + function.getNumOfParameters() + " parameters. " + numberOfPassedParameters + " parameters passed.");
                System.exit(1);
            } else if (!function.doesReturnValue() && returnValueRequired) {
                log.warning("fjp.Function '" + identifier + "' does not return value.");
                System.out.println("fjp.Function '" + identifier + "' does not return value.");
                System.exit(1);
            }
        }

        return functions.get(identifier);
    }
}
