package fjp;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import parser.FjpParser;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

class NumericTermParser {
    private List<String> instructions;

    private List<ParseTree> operators;
    private List<ParseTree> operands;
    private final static Logger log = Logger.getLogger(TreeListener.class.getName());

    NumericTermParser(List<ParseTree> operators, List<ParseTree> operands) {
        this.instructions = new ArrayList<>();
        this.operators = operators;
        this.operands = operands;
    }

    NumericTermParser() {
        this.instructions = new ArrayList<>();
    }

    void setTerm(List<ParseTree> operators, List<ParseTree> operands) {
        this.operators = operators;
        this.operands = operands;
    }

    /**
     *
     * @return Numeric term's instance set of instructions
     */
    List<String> getInstructions() {
        return instructions;
    }

    /**
     * Parses factors into a numeric term using postfix notation
     */
    void parse() {
        int index = 0;
        int operatorIndex = 0;

        while (index < operands.size()) {
            ParseTree operand = operands.get(index++);
            if (operand instanceof TerminalNode) {
                switch (((TerminalNode) operand).getSymbol().getType()) {
                    case FjpParser.NEG: /* should not be among operands since it's an operator but it's unary operator .. */
                        instructions.add(Instructions.createLit(TreeListener.FALSE));
                        instructions.add(Instructions.createEq());
                        break;
                    case FjpParser.IDENTIFIER:
                        Variable var = resolveVariable(operand.getText());
                        if (var.isValid()) {
                            int L = var.isGlobal() ? 1 : 0;
                            instructions.add(Instructions.createLod(L, var.getStackPosition()));
                        }
                        else if (CompilerState.constants.containsKey(operand.getText())) { /* might be a constant */
                            int constant = CompilerState.constants.get(operand.getText());
                            instructions.add(Instructions.createLit(constant));
                        }
                        else {
                            System.out.println("Undeclared variable " + operand.getText());
                            System.exit(1);
                        }
                        break;
                    case FjpParser.NUMBER:
                        instructions.add(Instructions.createLit(Integer.parseInt(operand.getText())));
                        break;
                    case FjpParser.BOOL_VAL:
                        if (operand.getText().equals(KeyWords.TRUE.getKeyWord())) {
                            instructions.add(Instructions.createLit(1));
                        }
                        else {
                            instructions.add(Instructions.createLit(0));
                        }
                        break;
                    default:
                        log.info("Default branch: might be a factor unit, ignoring ..");

                }
            }
            else if (operand instanceof FjpParser.Function_callContext) {
                FjpParser.Function_callContext funcCall = (FjpParser.Function_callContext) operand;
                Function function = Function.checkFunctionCall(funcCall, CompilerState.functions);

                instructions.add(Instructions.createCal(function.getFirstInstruction()));

                // Pop parameters from stack
                if (function.getNumOfParameters() != 0) {
                    instructions.add(Instructions.createInt(-function.getNumOfParameters()));
                }
            }

            if (index == 1) {
                continue;
            }

            if (operators.size() > 0) {
                instructions.add(Instructions.resolveOperator((TerminalNode) operators.get(operatorIndex++)));
            }
        }
    }

    /**
     * Resolves variable
     * - is valid
     * - position in stack
     * - is global
     * @param variableName name of variable to be resolved
     * @return fjp.Variable instance with according information
     */
    private Variable resolveVariable(String variableName) {
        int position;
        boolean isGlobal;

        if (!(CompilerState.localVariables.containsKey(variableName) || CompilerState.globalVariables.containsKey(variableName))) {
            return new Variable(false);
        }

        isGlobal = !CompilerState.localVariables.containsKey(variableName);
        position = isGlobal ? CompilerState.globalVariables.get(variableName) : CompilerState.localVariables.get(variableName);

        return new Variable(isGlobal, position);
    }
}
