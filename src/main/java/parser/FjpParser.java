package parser;
// Generated from ../Fjp.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FjpParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		CONST=1, INT=2, BOOL=3, VOID=4, SEMI=5, COMMA=6, COLON=7, QM=8, LPAREN=9, 
		RPAREN=10, LCURLY=11, RCURLY=12, MAIN=13, RETURN=14, IF=15, ELSE=16, WHILE=17, 
		DO=18, SWITCH=19, CASE=20, ASSIGN=21, EQ=22, NEQ=23, GT=24, LT=25, LE=26, 
		GE=27, NEG=28, AND=29, OR=30, ADD=31, SUB=32, MUL=33, DIV=34, NUMBER=35, 
		BOOL_VAL=36, IDENTIFIER=37, WS=38;
	public static final int
		RULE_translation_unit = 0, RULE_constant = 1, RULE_global_variables = 2, 
		RULE_variable = 3, RULE_function = 4, RULE_function_header = 5, RULE_function_parameters = 6, 
		RULE_function_parameter = 7, RULE_function_return = 8, RULE_main_function = 9, 
		RULE_block_body = 10, RULE_local_variables = 11, RULE_command = 12, RULE_switch_command = 13, 
		RULE_switch_condition = 14, RULE_switch_case = 15, RULE_switch_case_expression = 16, 
		RULE_while_cycle = 17, RULE_do_while = 18, RULE_if_clause = 19, RULE_else_clause = 20, 
		RULE_if_else_clause = 21, RULE_assignment = 22, RULE_ternar_assignment = 23, 
		RULE_ternar_identifier = 24, RULE_ternar_condition = 25, RULE_ternar_true = 26, 
		RULE_ternar_false = 27, RULE_parallel_assignment = 28, RULE_function_call = 29, 
		RULE_function_identifier = 30, RULE_expression = 31, RULE_logical_term = 32, 
		RULE_comparison = 33, RULE_numeric_expression = 34, RULE_numeric_term = 35, 
		RULE_factor = 36;
	public static final String[] ruleNames = {
		"translation_unit", "constant", "global_variables", "variable", "function", 
		"function_header", "function_parameters", "function_parameter", "function_return", 
		"main_function", "block_body", "local_variables", "command", "switch_command", 
		"switch_condition", "switch_case", "switch_case_expression", "while_cycle", 
		"do_while", "if_clause", "else_clause", "if_else_clause", "assignment", 
		"ternar_assignment", "ternar_identifier", "ternar_condition", "ternar_true", 
		"ternar_false", "parallel_assignment", "function_call", "function_identifier", 
		"expression", "logical_term", "comparison", "numeric_expression", "numeric_term", 
		"factor"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'const'", "'int'", "'bool'", "'void'", "';'", "','", "':'", "'?'", 
		"'('", "')'", "'{'", "'}'", "'main'", "'return'", "'if'", "'else'", "'while'", 
		"'do'", "'switch'", "'case'", "'='", "'=='", "'!='", "'>'", "'<'", "'<='", 
		"'>='", "'!'", "'&&'", "'||'", "'+'", "'-'", "'*'", "'/'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "CONST", "INT", "BOOL", "VOID", "SEMI", "COMMA", "COLON", "QM", 
		"LPAREN", "RPAREN", "LCURLY", "RCURLY", "MAIN", "RETURN", "IF", "ELSE", 
		"WHILE", "DO", "SWITCH", "CASE", "ASSIGN", "EQ", "NEQ", "GT", "LT", "LE", 
		"GE", "NEG", "AND", "OR", "ADD", "SUB", "MUL", "DIV", "NUMBER", "BOOL_VAL", 
		"IDENTIFIER", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Fjp.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public FjpParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class Translation_unitContext extends ParserRuleContext {
		public Global_variablesContext global_variables() {
			return getRuleContext(Global_variablesContext.class,0);
		}
		public Main_functionContext main_function() {
			return getRuleContext(Main_functionContext.class,0);
		}
		public List<ConstantContext> constant() {
			return getRuleContexts(ConstantContext.class);
		}
		public ConstantContext constant(int i) {
			return getRuleContext(ConstantContext.class,i);
		}
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(FjpParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(FjpParser.SEMI, i);
		}
		public List<FunctionContext> function() {
			return getRuleContexts(FunctionContext.class);
		}
		public FunctionContext function(int i) {
			return getRuleContext(FunctionContext.class,i);
		}
		public Translation_unitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_translation_unit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterTranslation_unit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitTranslation_unit(this);
		}
	}

	public final Translation_unitContext translation_unit() throws RecognitionException {
		Translation_unitContext _localctx = new Translation_unitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_translation_unit);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==CONST) {
				{
				{
				setState(74);
				constant();
				}
				}
				setState(79);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(80);
			global_variables();
			setState(86);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(81);
					variable();
					setState(82);
					match(SEMI);
					}
					} 
				}
				setState(88);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			setState(92);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(89);
					function();
					}
					} 
				}
				setState(94);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(95);
			main_function();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public TerminalNode CONST() { return getToken(FjpParser.CONST, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(FjpParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(FjpParser.IDENTIFIER, i);
		}
		public List<TerminalNode> ASSIGN() { return getTokens(FjpParser.ASSIGN); }
		public TerminalNode ASSIGN(int i) {
			return getToken(FjpParser.ASSIGN, i);
		}
		public List<TerminalNode> NUMBER() { return getTokens(FjpParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(FjpParser.NUMBER, i);
		}
		public TerminalNode SEMI() { return getToken(FjpParser.SEMI, 0); }
		public List<TerminalNode> COMMA() { return getTokens(FjpParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FjpParser.COMMA, i);
		}
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitConstant(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_constant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			match(CONST);
			setState(98);
			match(IDENTIFIER);
			setState(99);
			match(ASSIGN);
			setState(100);
			match(NUMBER);
			setState(107);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(101);
				match(COMMA);
				setState(102);
				match(IDENTIFIER);
				setState(103);
				match(ASSIGN);
				setState(104);
				match(NUMBER);
				}
				}
				setState(109);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(110);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Global_variablesContext extends ParserRuleContext {
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public Global_variablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_global_variables; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterGlobal_variables(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitGlobal_variables(this);
		}
	}

	public final Global_variablesContext global_variables() throws RecognitionException {
		Global_variablesContext _localctx = new Global_variablesContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_global_variables);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(112);
					variable();
					}
					} 
				}
				setState(117);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(FjpParser.INT, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(FjpParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(FjpParser.IDENTIFIER, i);
		}
		public TerminalNode SEMI() { return getToken(FjpParser.SEMI, 0); }
		public List<TerminalNode> COMMA() { return getTokens(FjpParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FjpParser.COMMA, i);
		}
		public TerminalNode BOOL() { return getToken(FjpParser.BOOL, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitVariable(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_variable);
		int _la;
		try {
			setState(138);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(118);
				match(INT);
				setState(119);
				match(IDENTIFIER);
				setState(124);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(120);
					match(COMMA);
					setState(121);
					match(IDENTIFIER);
					}
					}
					setState(126);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(127);
				match(SEMI);
				}
				break;
			case BOOL:
				enterOuterAlt(_localctx, 2);
				{
				setState(128);
				match(BOOL);
				setState(129);
				match(IDENTIFIER);
				setState(134);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(130);
					match(COMMA);
					setState(131);
					match(IDENTIFIER);
					}
					}
					setState(136);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(137);
				match(SEMI);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public Function_headerContext function_header() {
			return getRuleContext(Function_headerContext.class,0);
		}
		public TerminalNode LCURLY() { return getToken(FjpParser.LCURLY, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public TerminalNode RCURLY() { return getToken(FjpParser.RCURLY, 0); }
		public Function_returnContext function_return() {
			return getRuleContext(Function_returnContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(FjpParser.SEMI, 0); }
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitFunction(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			function_header();
			setState(141);
			match(LCURLY);
			setState(142);
			block_body();
			setState(146);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(143);
				function_return();
				setState(144);
				match(SEMI);
				}
			}

			setState(148);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_headerContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(FjpParser.IDENTIFIER, 0); }
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public Function_parametersContext function_parameters() {
			return getRuleContext(Function_parametersContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public TerminalNode INT() { return getToken(FjpParser.INT, 0); }
		public TerminalNode BOOL() { return getToken(FjpParser.BOOL, 0); }
		public TerminalNode VOID() { return getToken(FjpParser.VOID, 0); }
		public Function_headerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_header; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterFunction_header(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitFunction_header(this);
		}
	}

	public final Function_headerContext function_header() throws RecognitionException {
		Function_headerContext _localctx = new Function_headerContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_function_header);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(150);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << BOOL) | (1L << VOID))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(151);
			match(IDENTIFIER);
			setState(152);
			match(LPAREN);
			setState(153);
			function_parameters();
			setState(154);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_parametersContext extends ParserRuleContext {
		public List<Function_parameterContext> function_parameter() {
			return getRuleContexts(Function_parameterContext.class);
		}
		public Function_parameterContext function_parameter(int i) {
			return getRuleContext(Function_parameterContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FjpParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FjpParser.COMMA, i);
		}
		public Function_parametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_parameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterFunction_parameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitFunction_parameters(this);
		}
	}

	public final Function_parametersContext function_parameters() throws RecognitionException {
		Function_parametersContext _localctx = new Function_parametersContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_function_parameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==INT || _la==BOOL) {
				{
				setState(156);
				function_parameter();
				setState(161);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(157);
					match(COMMA);
					setState(158);
					function_parameter();
					}
					}
					setState(163);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_parameterContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(FjpParser.INT, 0); }
		public TerminalNode IDENTIFIER() { return getToken(FjpParser.IDENTIFIER, 0); }
		public TerminalNode BOOL() { return getToken(FjpParser.BOOL, 0); }
		public Function_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterFunction_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitFunction_parameter(this);
		}
	}

	public final Function_parameterContext function_parameter() throws RecognitionException {
		Function_parameterContext _localctx = new Function_parameterContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_function_parameter);
		try {
			setState(170);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(166);
				match(INT);
				setState(167);
				match(IDENTIFIER);
				}
				break;
			case BOOL:
				enterOuterAlt(_localctx, 2);
				{
				setState(168);
				match(BOOL);
				setState(169);
				match(IDENTIFIER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_returnContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(FjpParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Function_returnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_return; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterFunction_return(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitFunction_return(this);
		}
	}

	public final Function_returnContext function_return() throws RecognitionException {
		Function_returnContext _localctx = new Function_returnContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_function_return);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(172);
			match(RETURN);
			setState(173);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Main_functionContext extends ParserRuleContext {
		public TerminalNode VOID() { return getToken(FjpParser.VOID, 0); }
		public TerminalNode MAIN() { return getToken(FjpParser.MAIN, 0); }
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public TerminalNode LCURLY() { return getToken(FjpParser.LCURLY, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public TerminalNode RCURLY() { return getToken(FjpParser.RCURLY, 0); }
		public Main_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_main_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterMain_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitMain_function(this);
		}
	}

	public final Main_functionContext main_function() throws RecognitionException {
		Main_functionContext _localctx = new Main_functionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_main_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(175);
			match(VOID);
			setState(176);
			match(MAIN);
			setState(177);
			match(LPAREN);
			setState(178);
			match(RPAREN);
			setState(179);
			match(LCURLY);
			setState(180);
			block_body();
			setState(181);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Block_bodyContext extends ParserRuleContext {
		public Local_variablesContext local_variables() {
			return getRuleContext(Local_variablesContext.class,0);
		}
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public Block_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterBlock_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitBlock_body(this);
		}
	}

	public final Block_bodyContext block_body() throws RecognitionException {
		Block_bodyContext _localctx = new Block_bodyContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_block_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(183);
			local_variables();
			setState(187);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << IF) | (1L << WHILE) | (1L << DO) | (1L << SWITCH) | (1L << IDENTIFIER))) != 0)) {
				{
				{
				setState(184);
				command();
				}
				}
				setState(189);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Local_variablesContext extends ParserRuleContext {
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public Local_variablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_local_variables; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterLocal_variables(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitLocal_variables(this);
		}
	}

	public final Local_variablesContext local_variables() throws RecognitionException {
		Local_variablesContext _localctx = new Local_variablesContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_local_variables);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==INT || _la==BOOL) {
				{
				{
				setState(190);
				variable();
				}
				}
				setState(195);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(FjpParser.SEMI, 0); }
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public If_clauseContext if_clause() {
			return getRuleContext(If_clauseContext.class,0);
		}
		public If_else_clauseContext if_else_clause() {
			return getRuleContext(If_else_clauseContext.class,0);
		}
		public While_cycleContext while_cycle() {
			return getRuleContext(While_cycleContext.class,0);
		}
		public Do_whileContext do_while() {
			return getRuleContext(Do_whileContext.class,0);
		}
		public Switch_commandContext switch_command() {
			return getRuleContext(Switch_commandContext.class,0);
		}
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitCommand(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_command);
		try {
			setState(207);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(196);
				assignment();
				setState(197);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(199);
				function_call();
				setState(200);
				match(SEMI);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(202);
				if_clause();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(203);
				if_else_clause();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(204);
				while_cycle();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(205);
				do_while();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(206);
				switch_command();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_commandContext extends ParserRuleContext {
		public TerminalNode SWITCH() { return getToken(FjpParser.SWITCH, 0); }
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public Switch_conditionContext switch_condition() {
			return getRuleContext(Switch_conditionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public TerminalNode LCURLY() { return getToken(FjpParser.LCURLY, 0); }
		public TerminalNode RCURLY() { return getToken(FjpParser.RCURLY, 0); }
		public List<Switch_caseContext> switch_case() {
			return getRuleContexts(Switch_caseContext.class);
		}
		public Switch_caseContext switch_case(int i) {
			return getRuleContext(Switch_caseContext.class,i);
		}
		public Switch_commandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_command; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterSwitch_command(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitSwitch_command(this);
		}
	}

	public final Switch_commandContext switch_command() throws RecognitionException {
		Switch_commandContext _localctx = new Switch_commandContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_switch_command);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(209);
			match(SWITCH);
			setState(210);
			match(LPAREN);
			setState(211);
			switch_condition();
			setState(212);
			match(RPAREN);
			setState(213);
			match(LCURLY);
			setState(217);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==CASE) {
				{
				{
				setState(214);
				switch_case();
				}
				}
				setState(219);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(220);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_conditionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Switch_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterSwitch_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitSwitch_condition(this);
		}
	}

	public final Switch_conditionContext switch_condition() throws RecognitionException {
		Switch_conditionContext _localctx = new Switch_conditionContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_switch_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(222);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_caseContext extends ParserRuleContext {
		public TerminalNode CASE() { return getToken(FjpParser.CASE, 0); }
		public Switch_case_expressionContext switch_case_expression() {
			return getRuleContext(Switch_case_expressionContext.class,0);
		}
		public TerminalNode COLON() { return getToken(FjpParser.COLON, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public Switch_caseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_case; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterSwitch_case(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitSwitch_case(this);
		}
	}

	public final Switch_caseContext switch_case() throws RecognitionException {
		Switch_caseContext _localctx = new Switch_caseContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_switch_case);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(224);
			match(CASE);
			setState(225);
			switch_case_expression();
			setState(226);
			match(COLON);
			setState(227);
			block_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_case_expressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Switch_case_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_case_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterSwitch_case_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitSwitch_case_expression(this);
		}
	}

	public final Switch_case_expressionContext switch_case_expression() throws RecognitionException {
		Switch_case_expressionContext _localctx = new Switch_case_expressionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_switch_case_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_cycleContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(FjpParser.WHILE, 0); }
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public TerminalNode LCURLY() { return getToken(FjpParser.LCURLY, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public TerminalNode RCURLY() { return getToken(FjpParser.RCURLY, 0); }
		public While_cycleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_cycle; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterWhile_cycle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitWhile_cycle(this);
		}
	}

	public final While_cycleContext while_cycle() throws RecognitionException {
		While_cycleContext _localctx = new While_cycleContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_while_cycle);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(231);
			match(WHILE);
			setState(232);
			match(LPAREN);
			setState(233);
			expression();
			setState(234);
			match(RPAREN);
			setState(235);
			match(LCURLY);
			setState(236);
			block_body();
			setState(237);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Do_whileContext extends ParserRuleContext {
		public TerminalNode DO() { return getToken(FjpParser.DO, 0); }
		public TerminalNode LCURLY() { return getToken(FjpParser.LCURLY, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public TerminalNode RCURLY() { return getToken(FjpParser.RCURLY, 0); }
		public TerminalNode WHILE() { return getToken(FjpParser.WHILE, 0); }
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public TerminalNode SEMI() { return getToken(FjpParser.SEMI, 0); }
		public Do_whileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_do_while; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterDo_while(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitDo_while(this);
		}
	}

	public final Do_whileContext do_while() throws RecognitionException {
		Do_whileContext _localctx = new Do_whileContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_do_while);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239);
			match(DO);
			setState(240);
			match(LCURLY);
			setState(241);
			block_body();
			setState(242);
			match(RCURLY);
			setState(243);
			match(WHILE);
			setState(244);
			match(LPAREN);
			setState(245);
			expression();
			setState(246);
			match(RPAREN);
			setState(247);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_clauseContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(FjpParser.IF, 0); }
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public TerminalNode LCURLY() { return getToken(FjpParser.LCURLY, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public TerminalNode RCURLY() { return getToken(FjpParser.RCURLY, 0); }
		public If_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterIf_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitIf_clause(this);
		}
	}

	public final If_clauseContext if_clause() throws RecognitionException {
		If_clauseContext _localctx = new If_clauseContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_if_clause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(249);
			match(IF);
			setState(250);
			match(LPAREN);
			setState(251);
			expression();
			setState(252);
			match(RPAREN);
			setState(253);
			match(LCURLY);
			setState(254);
			block_body();
			setState(255);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_clauseContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(FjpParser.ELSE, 0); }
		public TerminalNode LCURLY() { return getToken(FjpParser.LCURLY, 0); }
		public Block_bodyContext block_body() {
			return getRuleContext(Block_bodyContext.class,0);
		}
		public TerminalNode RCURLY() { return getToken(FjpParser.RCURLY, 0); }
		public Else_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterElse_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitElse_clause(this);
		}
	}

	public final Else_clauseContext else_clause() throws RecognitionException {
		Else_clauseContext _localctx = new Else_clauseContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_else_clause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(257);
			match(ELSE);
			setState(258);
			match(LCURLY);
			setState(259);
			block_body();
			setState(260);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_else_clauseContext extends ParserRuleContext {
		public If_clauseContext if_clause() {
			return getRuleContext(If_clauseContext.class,0);
		}
		public Else_clauseContext else_clause() {
			return getRuleContext(Else_clauseContext.class,0);
		}
		public If_else_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_else_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterIf_else_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitIf_else_clause(this);
		}
	}

	public final If_else_clauseContext if_else_clause() throws RecognitionException {
		If_else_clauseContext _localctx = new If_else_clauseContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_if_else_clause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			if_clause();
			setState(263);
			else_clause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(FjpParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(FjpParser.IDENTIFIER, i);
		}
		public List<TerminalNode> ASSIGN() { return getTokens(FjpParser.ASSIGN); }
		public TerminalNode ASSIGN(int i) {
			return getToken(FjpParser.ASSIGN, i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Ternar_assignmentContext ternar_assignment() {
			return getRuleContext(Ternar_assignmentContext.class,0);
		}
		public Parallel_assignmentContext parallel_assignment() {
			return getRuleContext(Parallel_assignmentContext.class,0);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitAssignment(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_assignment);
		try {
			int _alt;
			setState(277);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(265);
				match(IDENTIFIER);
				setState(266);
				match(ASSIGN);
				setState(271);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(267);
						match(IDENTIFIER);
						setState(268);
						match(ASSIGN);
						}
						} 
					}
					setState(273);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
				}
				setState(274);
				expression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(275);
				ternar_assignment();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(276);
				parallel_assignment();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ternar_assignmentContext extends ParserRuleContext {
		public Ternar_identifierContext ternar_identifier() {
			return getRuleContext(Ternar_identifierContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(FjpParser.ASSIGN, 0); }
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public Ternar_conditionContext ternar_condition() {
			return getRuleContext(Ternar_conditionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public TerminalNode QM() { return getToken(FjpParser.QM, 0); }
		public Ternar_trueContext ternar_true() {
			return getRuleContext(Ternar_trueContext.class,0);
		}
		public TerminalNode COLON() { return getToken(FjpParser.COLON, 0); }
		public Ternar_falseContext ternar_false() {
			return getRuleContext(Ternar_falseContext.class,0);
		}
		public Ternar_assignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ternar_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterTernar_assignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitTernar_assignment(this);
		}
	}

	public final Ternar_assignmentContext ternar_assignment() throws RecognitionException {
		Ternar_assignmentContext _localctx = new Ternar_assignmentContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_ternar_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(279);
			ternar_identifier();
			setState(280);
			match(ASSIGN);
			setState(281);
			match(LPAREN);
			setState(282);
			ternar_condition();
			setState(283);
			match(RPAREN);
			setState(284);
			match(QM);
			setState(285);
			ternar_true();
			setState(286);
			match(COLON);
			setState(287);
			ternar_false();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ternar_identifierContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(FjpParser.IDENTIFIER, 0); }
		public Ternar_identifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ternar_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterTernar_identifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitTernar_identifier(this);
		}
	}

	public final Ternar_identifierContext ternar_identifier() throws RecognitionException {
		Ternar_identifierContext _localctx = new Ternar_identifierContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_ternar_identifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(289);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ternar_conditionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Ternar_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ternar_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterTernar_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitTernar_condition(this);
		}
	}

	public final Ternar_conditionContext ternar_condition() throws RecognitionException {
		Ternar_conditionContext _localctx = new Ternar_conditionContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_ternar_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(291);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ternar_trueContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Ternar_trueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ternar_true; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterTernar_true(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitTernar_true(this);
		}
	}

	public final Ternar_trueContext ternar_true() throws RecognitionException {
		Ternar_trueContext _localctx = new Ternar_trueContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_ternar_true);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(293);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ternar_falseContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Ternar_falseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ternar_false; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterTernar_false(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitTernar_false(this);
		}
	}

	public final Ternar_falseContext ternar_false() throws RecognitionException {
		Ternar_falseContext _localctx = new Ternar_falseContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_ternar_false);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(295);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parallel_assignmentContext extends ParserRuleContext {
		public List<TerminalNode> LCURLY() { return getTokens(FjpParser.LCURLY); }
		public TerminalNode LCURLY(int i) {
			return getToken(FjpParser.LCURLY, i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(FjpParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(FjpParser.IDENTIFIER, i);
		}
		public List<TerminalNode> RCURLY() { return getTokens(FjpParser.RCURLY); }
		public TerminalNode RCURLY(int i) {
			return getToken(FjpParser.RCURLY, i);
		}
		public TerminalNode ASSIGN() { return getToken(FjpParser.ASSIGN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FjpParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FjpParser.COMMA, i);
		}
		public Parallel_assignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parallel_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterParallel_assignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitParallel_assignment(this);
		}
	}

	public final Parallel_assignmentContext parallel_assignment() throws RecognitionException {
		Parallel_assignmentContext _localctx = new Parallel_assignmentContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_parallel_assignment);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(297);
			match(LCURLY);
			setState(298);
			match(IDENTIFIER);
			setState(303);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(299);
				match(COMMA);
				setState(300);
				match(IDENTIFIER);
				}
				}
				setState(305);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(306);
			match(RCURLY);
			setState(307);
			match(ASSIGN);
			setState(308);
			match(LCURLY);
			setState(309);
			expression();
			setState(314);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(310);
				match(COMMA);
				setState(311);
				expression();
				}
				}
				setState(316);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(317);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_callContext extends ParserRuleContext {
		public Function_identifierContext function_identifier() {
			return getRuleContext(Function_identifierContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(FjpParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(FjpParser.COMMA, i);
		}
		public Function_callContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterFunction_call(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitFunction_call(this);
		}
	}

	public final Function_callContext function_call() throws RecognitionException {
		Function_callContext _localctx = new Function_callContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_function_call);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(319);
			function_identifier();
			setState(320);
			match(LPAREN);
			setState(329);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LPAREN) | (1L << NEG) | (1L << ADD) | (1L << SUB) | (1L << NUMBER) | (1L << BOOL_VAL) | (1L << IDENTIFIER))) != 0)) {
				{
				setState(321);
				expression();
				setState(326);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(322);
					match(COMMA);
					setState(323);
					expression();
					}
					}
					setState(328);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(331);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_identifierContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(FjpParser.IDENTIFIER, 0); }
		public Function_identifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterFunction_identifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitFunction_identifier(this);
		}
	}

	public final Function_identifierContext function_identifier() throws RecognitionException {
		Function_identifierContext _localctx = new Function_identifierContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_function_identifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(333);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public List<Logical_termContext> logical_term() {
			return getRuleContexts(Logical_termContext.class);
		}
		public Logical_termContext logical_term(int i) {
			return getRuleContext(Logical_termContext.class,i);
		}
		public List<TerminalNode> OR() { return getTokens(FjpParser.OR); }
		public TerminalNode OR(int i) {
			return getToken(FjpParser.OR, i);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(335);
			logical_term();
			setState(340);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OR) {
				{
				{
				setState(336);
				match(OR);
				setState(337);
				logical_term();
				}
				}
				setState(342);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_termContext extends ParserRuleContext {
		public List<ComparisonContext> comparison() {
			return getRuleContexts(ComparisonContext.class);
		}
		public ComparisonContext comparison(int i) {
			return getRuleContext(ComparisonContext.class,i);
		}
		public List<TerminalNode> AND() { return getTokens(FjpParser.AND); }
		public TerminalNode AND(int i) {
			return getToken(FjpParser.AND, i);
		}
		public Logical_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterLogical_term(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitLogical_term(this);
		}
	}

	public final Logical_termContext logical_term() throws RecognitionException {
		Logical_termContext _localctx = new Logical_termContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_logical_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(343);
			comparison();
			setState(348);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AND) {
				{
				{
				setState(344);
				match(AND);
				setState(345);
				comparison();
				}
				}
				setState(350);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComparisonContext extends ParserRuleContext {
		public List<Numeric_expressionContext> numeric_expression() {
			return getRuleContexts(Numeric_expressionContext.class);
		}
		public Numeric_expressionContext numeric_expression(int i) {
			return getRuleContext(Numeric_expressionContext.class,i);
		}
		public List<TerminalNode> EQ() { return getTokens(FjpParser.EQ); }
		public TerminalNode EQ(int i) {
			return getToken(FjpParser.EQ, i);
		}
		public List<TerminalNode> NEQ() { return getTokens(FjpParser.NEQ); }
		public TerminalNode NEQ(int i) {
			return getToken(FjpParser.NEQ, i);
		}
		public List<TerminalNode> LT() { return getTokens(FjpParser.LT); }
		public TerminalNode LT(int i) {
			return getToken(FjpParser.LT, i);
		}
		public List<TerminalNode> LE() { return getTokens(FjpParser.LE); }
		public TerminalNode LE(int i) {
			return getToken(FjpParser.LE, i);
		}
		public List<TerminalNode> GT() { return getTokens(FjpParser.GT); }
		public TerminalNode GT(int i) {
			return getToken(FjpParser.GT, i);
		}
		public List<TerminalNode> GE() { return getTokens(FjpParser.GE); }
		public TerminalNode GE(int i) {
			return getToken(FjpParser.GE, i);
		}
		public ComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitComparison(this);
		}
	}

	public final ComparisonContext comparison() throws RecognitionException {
		ComparisonContext _localctx = new ComparisonContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_comparison);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(351);
			numeric_expression();
			setState(356);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << NEQ) | (1L << GT) | (1L << LT) | (1L << LE) | (1L << GE))) != 0)) {
				{
				{
				setState(352);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << NEQ) | (1L << GT) | (1L << LT) | (1L << LE) | (1L << GE))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(353);
				numeric_expression();
				}
				}
				setState(358);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Numeric_expressionContext extends ParserRuleContext {
		public List<Numeric_termContext> numeric_term() {
			return getRuleContexts(Numeric_termContext.class);
		}
		public Numeric_termContext numeric_term(int i) {
			return getRuleContext(Numeric_termContext.class,i);
		}
		public List<TerminalNode> ADD() { return getTokens(FjpParser.ADD); }
		public TerminalNode ADD(int i) {
			return getToken(FjpParser.ADD, i);
		}
		public List<TerminalNode> SUB() { return getTokens(FjpParser.SUB); }
		public TerminalNode SUB(int i) {
			return getToken(FjpParser.SUB, i);
		}
		public Numeric_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numeric_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterNumeric_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitNumeric_expression(this);
		}
	}

	public final Numeric_expressionContext numeric_expression() throws RecognitionException {
		Numeric_expressionContext _localctx = new Numeric_expressionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_numeric_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(360);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ADD || _la==SUB) {
				{
				setState(359);
				_la = _input.LA(1);
				if ( !(_la==ADD || _la==SUB) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(362);
			numeric_term();
			setState(367);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ADD || _la==SUB) {
				{
				{
				setState(363);
				_la = _input.LA(1);
				if ( !(_la==ADD || _la==SUB) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(364);
				numeric_term();
				}
				}
				setState(369);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Numeric_termContext extends ParserRuleContext {
		public List<FactorContext> factor() {
			return getRuleContexts(FactorContext.class);
		}
		public FactorContext factor(int i) {
			return getRuleContext(FactorContext.class,i);
		}
		public List<TerminalNode> MUL() { return getTokens(FjpParser.MUL); }
		public TerminalNode MUL(int i) {
			return getToken(FjpParser.MUL, i);
		}
		public List<TerminalNode> DIV() { return getTokens(FjpParser.DIV); }
		public TerminalNode DIV(int i) {
			return getToken(FjpParser.DIV, i);
		}
		public Numeric_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numeric_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterNumeric_term(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitNumeric_term(this);
		}
	}

	public final Numeric_termContext numeric_term() throws RecognitionException {
		Numeric_termContext _localctx = new Numeric_termContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_numeric_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(370);
			factor();
			setState(375);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==MUL || _la==DIV) {
				{
				{
				setState(371);
				_la = _input.LA(1);
				if ( !(_la==MUL || _la==DIV) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(372);
				factor();
				}
				}
				setState(377);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(FjpParser.IDENTIFIER, 0); }
		public TerminalNode NUMBER() { return getToken(FjpParser.NUMBER, 0); }
		public TerminalNode LPAREN() { return getToken(FjpParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(FjpParser.RPAREN, 0); }
		public TerminalNode BOOL_VAL() { return getToken(FjpParser.BOOL_VAL, 0); }
		public Function_callContext function_call() {
			return getRuleContext(Function_callContext.class,0);
		}
		public TerminalNode NEG() { return getToken(FjpParser.NEG, 0); }
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).enterFactor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FjpListener ) ((FjpListener)listener).exitFactor(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_factor);
		try {
			setState(388);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(378);
				match(IDENTIFIER);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(379);
				match(NUMBER);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(380);
				match(LPAREN);
				setState(381);
				expression();
				setState(382);
				match(RPAREN);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(384);
				match(BOOL_VAL);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(385);
				function_call();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(386);
				match(NEG);
				setState(387);
				factor();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3(\u0189\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\3\2\7\2N\n\2\f\2\16\2Q\13\2\3\2\3"+
		"\2\3\2\3\2\7\2W\n\2\f\2\16\2Z\13\2\3\2\7\2]\n\2\f\2\16\2`\13\2\3\2\3\2"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3l\n\3\f\3\16\3o\13\3\3\3\3\3\3\4\7"+
		"\4t\n\4\f\4\16\4w\13\4\3\5\3\5\3\5\3\5\7\5}\n\5\f\5\16\5\u0080\13\5\3"+
		"\5\3\5\3\5\3\5\3\5\7\5\u0087\n\5\f\5\16\5\u008a\13\5\3\5\5\5\u008d\n\5"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u0095\n\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\b\3\b\3\b\7\b\u00a2\n\b\f\b\16\b\u00a5\13\b\5\b\u00a7\n\b\3\t\3\t\3"+
		"\t\3\t\5\t\u00ad\n\t\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\f\3\f\7\f\u00bc\n\f\f\f\16\f\u00bf\13\f\3\r\7\r\u00c2\n\r\f\r\16"+
		"\r\u00c5\13\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5"+
		"\16\u00d2\n\16\3\17\3\17\3\17\3\17\3\17\3\17\7\17\u00da\n\17\f\17\16\17"+
		"\u00dd\13\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3"+
		"\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3"+
		"\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\30\3\30\7\30\u0110\n\30\f\30"+
		"\16\30\u0113\13\30\3\30\3\30\3\30\5\30\u0118\n\30\3\31\3\31\3\31\3\31"+
		"\3\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35"+
		"\3\36\3\36\3\36\3\36\7\36\u0130\n\36\f\36\16\36\u0133\13\36\3\36\3\36"+
		"\3\36\3\36\3\36\3\36\7\36\u013b\n\36\f\36\16\36\u013e\13\36\3\36\3\36"+
		"\3\37\3\37\3\37\3\37\3\37\7\37\u0147\n\37\f\37\16\37\u014a\13\37\5\37"+
		"\u014c\n\37\3\37\3\37\3 \3 \3!\3!\3!\7!\u0155\n!\f!\16!\u0158\13!\3\""+
		"\3\"\3\"\7\"\u015d\n\"\f\"\16\"\u0160\13\"\3#\3#\3#\7#\u0165\n#\f#\16"+
		"#\u0168\13#\3$\5$\u016b\n$\3$\3$\3$\7$\u0170\n$\f$\16$\u0173\13$\3%\3"+
		"%\3%\7%\u0178\n%\f%\16%\u017b\13%\3&\3&\3&\3&\3&\3&\3&\3&\3&\3&\5&\u0187"+
		"\n&\3&\2\2\'\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64"+
		"\668:<>@BDFHJ\2\6\3\2\4\6\3\2\30\35\3\2!\"\3\2#$\2\u018a\2O\3\2\2\2\4"+
		"c\3\2\2\2\6u\3\2\2\2\b\u008c\3\2\2\2\n\u008e\3\2\2\2\f\u0098\3\2\2\2\16"+
		"\u00a6\3\2\2\2\20\u00ac\3\2\2\2\22\u00ae\3\2\2\2\24\u00b1\3\2\2\2\26\u00b9"+
		"\3\2\2\2\30\u00c3\3\2\2\2\32\u00d1\3\2\2\2\34\u00d3\3\2\2\2\36\u00e0\3"+
		"\2\2\2 \u00e2\3\2\2\2\"\u00e7\3\2\2\2$\u00e9\3\2\2\2&\u00f1\3\2\2\2(\u00fb"+
		"\3\2\2\2*\u0103\3\2\2\2,\u0108\3\2\2\2.\u0117\3\2\2\2\60\u0119\3\2\2\2"+
		"\62\u0123\3\2\2\2\64\u0125\3\2\2\2\66\u0127\3\2\2\28\u0129\3\2\2\2:\u012b"+
		"\3\2\2\2<\u0141\3\2\2\2>\u014f\3\2\2\2@\u0151\3\2\2\2B\u0159\3\2\2\2D"+
		"\u0161\3\2\2\2F\u016a\3\2\2\2H\u0174\3\2\2\2J\u0186\3\2\2\2LN\5\4\3\2"+
		"ML\3\2\2\2NQ\3\2\2\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO\3\2\2\2RX\5\6\4\2"+
		"ST\5\b\5\2TU\7\7\2\2UW\3\2\2\2VS\3\2\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2"+
		"Y^\3\2\2\2ZX\3\2\2\2[]\5\n\6\2\\[\3\2\2\2]`\3\2\2\2^\\\3\2\2\2^_\3\2\2"+
		"\2_a\3\2\2\2`^\3\2\2\2ab\5\24\13\2b\3\3\2\2\2cd\7\3\2\2de\7\'\2\2ef\7"+
		"\27\2\2fm\7%\2\2gh\7\b\2\2hi\7\'\2\2ij\7\27\2\2jl\7%\2\2kg\3\2\2\2lo\3"+
		"\2\2\2mk\3\2\2\2mn\3\2\2\2np\3\2\2\2om\3\2\2\2pq\7\7\2\2q\5\3\2\2\2rt"+
		"\5\b\5\2sr\3\2\2\2tw\3\2\2\2us\3\2\2\2uv\3\2\2\2v\7\3\2\2\2wu\3\2\2\2"+
		"xy\7\4\2\2y~\7\'\2\2z{\7\b\2\2{}\7\'\2\2|z\3\2\2\2}\u0080\3\2\2\2~|\3"+
		"\2\2\2~\177\3\2\2\2\177\u0081\3\2\2\2\u0080~\3\2\2\2\u0081\u008d\7\7\2"+
		"\2\u0082\u0083\7\5\2\2\u0083\u0088\7\'\2\2\u0084\u0085\7\b\2\2\u0085\u0087"+
		"\7\'\2\2\u0086\u0084\3\2\2\2\u0087\u008a\3\2\2\2\u0088\u0086\3\2\2\2\u0088"+
		"\u0089\3\2\2\2\u0089\u008b\3\2\2\2\u008a\u0088\3\2\2\2\u008b\u008d\7\7"+
		"\2\2\u008cx\3\2\2\2\u008c\u0082\3\2\2\2\u008d\t\3\2\2\2\u008e\u008f\5"+
		"\f\7\2\u008f\u0090\7\r\2\2\u0090\u0094\5\26\f\2\u0091\u0092\5\22\n\2\u0092"+
		"\u0093\7\7\2\2\u0093\u0095\3\2\2\2\u0094\u0091\3\2\2\2\u0094\u0095\3\2"+
		"\2\2\u0095\u0096\3\2\2\2\u0096\u0097\7\16\2\2\u0097\13\3\2\2\2\u0098\u0099"+
		"\t\2\2\2\u0099\u009a\7\'\2\2\u009a\u009b\7\13\2\2\u009b\u009c\5\16\b\2"+
		"\u009c\u009d\7\f\2\2\u009d\r\3\2\2\2\u009e\u00a3\5\20\t\2\u009f\u00a0"+
		"\7\b\2\2\u00a0\u00a2\5\20\t\2\u00a1\u009f\3\2\2\2\u00a2\u00a5\3\2\2\2"+
		"\u00a3\u00a1\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a7\3\2\2\2\u00a5\u00a3"+
		"\3\2\2\2\u00a6\u009e\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\17\3\2\2\2\u00a8"+
		"\u00a9\7\4\2\2\u00a9\u00ad\7\'\2\2\u00aa\u00ab\7\5\2\2\u00ab\u00ad\7\'"+
		"\2\2\u00ac\u00a8\3\2\2\2\u00ac\u00aa\3\2\2\2\u00ad\21\3\2\2\2\u00ae\u00af"+
		"\7\20\2\2\u00af\u00b0\5@!\2\u00b0\23\3\2\2\2\u00b1\u00b2\7\6\2\2\u00b2"+
		"\u00b3\7\17\2\2\u00b3\u00b4\7\13\2\2\u00b4\u00b5\7\f\2\2\u00b5\u00b6\7"+
		"\r\2\2\u00b6\u00b7\5\26\f\2\u00b7\u00b8\7\16\2\2\u00b8\25\3\2\2\2\u00b9"+
		"\u00bd\5\30\r\2\u00ba\u00bc\5\32\16\2\u00bb\u00ba\3\2\2\2\u00bc\u00bf"+
		"\3\2\2\2\u00bd\u00bb\3\2\2\2\u00bd\u00be\3\2\2\2\u00be\27\3\2\2\2\u00bf"+
		"\u00bd\3\2\2\2\u00c0\u00c2\5\b\5\2\u00c1\u00c0\3\2\2\2\u00c2\u00c5\3\2"+
		"\2\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\31\3\2\2\2\u00c5\u00c3"+
		"\3\2\2\2\u00c6\u00c7\5.\30\2\u00c7\u00c8\7\7\2\2\u00c8\u00d2\3\2\2\2\u00c9"+
		"\u00ca\5<\37\2\u00ca\u00cb\7\7\2\2\u00cb\u00d2\3\2\2\2\u00cc\u00d2\5("+
		"\25\2\u00cd\u00d2\5,\27\2\u00ce\u00d2\5$\23\2\u00cf\u00d2\5&\24\2\u00d0"+
		"\u00d2\5\34\17\2\u00d1\u00c6\3\2\2\2\u00d1\u00c9\3\2\2\2\u00d1\u00cc\3"+
		"\2\2\2\u00d1\u00cd\3\2\2\2\u00d1\u00ce\3\2\2\2\u00d1\u00cf\3\2\2\2\u00d1"+
		"\u00d0\3\2\2\2\u00d2\33\3\2\2\2\u00d3\u00d4\7\25\2\2\u00d4\u00d5\7\13"+
		"\2\2\u00d5\u00d6\5\36\20\2\u00d6\u00d7\7\f\2\2\u00d7\u00db\7\r\2\2\u00d8"+
		"\u00da\5 \21\2\u00d9\u00d8\3\2\2\2\u00da\u00dd\3\2\2\2\u00db\u00d9\3\2"+
		"\2\2\u00db\u00dc\3\2\2\2\u00dc\u00de\3\2\2\2\u00dd\u00db\3\2\2\2\u00de"+
		"\u00df\7\16\2\2\u00df\35\3\2\2\2\u00e0\u00e1\5@!\2\u00e1\37\3\2\2\2\u00e2"+
		"\u00e3\7\26\2\2\u00e3\u00e4\5\"\22\2\u00e4\u00e5\7\t\2\2\u00e5\u00e6\5"+
		"\26\f\2\u00e6!\3\2\2\2\u00e7\u00e8\5@!\2\u00e8#\3\2\2\2\u00e9\u00ea\7"+
		"\23\2\2\u00ea\u00eb\7\13\2\2\u00eb\u00ec\5@!\2\u00ec\u00ed\7\f\2\2\u00ed"+
		"\u00ee\7\r\2\2\u00ee\u00ef\5\26\f\2\u00ef\u00f0\7\16\2\2\u00f0%\3\2\2"+
		"\2\u00f1\u00f2\7\24\2\2\u00f2\u00f3\7\r\2\2\u00f3\u00f4\5\26\f\2\u00f4"+
		"\u00f5\7\16\2\2\u00f5\u00f6\7\23\2\2\u00f6\u00f7\7\13\2\2\u00f7\u00f8"+
		"\5@!\2\u00f8\u00f9\7\f\2\2\u00f9\u00fa\7\7\2\2\u00fa\'\3\2\2\2\u00fb\u00fc"+
		"\7\21\2\2\u00fc\u00fd\7\13\2\2\u00fd\u00fe\5@!\2\u00fe\u00ff\7\f\2\2\u00ff"+
		"\u0100\7\r\2\2\u0100\u0101\5\26\f\2\u0101\u0102\7\16\2\2\u0102)\3\2\2"+
		"\2\u0103\u0104\7\22\2\2\u0104\u0105\7\r\2\2\u0105\u0106\5\26\f\2\u0106"+
		"\u0107\7\16\2\2\u0107+\3\2\2\2\u0108\u0109\5(\25\2\u0109\u010a\5*\26\2"+
		"\u010a-\3\2\2\2\u010b\u010c\7\'\2\2\u010c\u0111\7\27\2\2\u010d\u010e\7"+
		"\'\2\2\u010e\u0110\7\27\2\2\u010f\u010d\3\2\2\2\u0110\u0113\3\2\2\2\u0111"+
		"\u010f\3\2\2\2\u0111\u0112\3\2\2\2\u0112\u0114\3\2\2\2\u0113\u0111\3\2"+
		"\2\2\u0114\u0118\5@!\2\u0115\u0118\5\60\31\2\u0116\u0118\5:\36\2\u0117"+
		"\u010b\3\2\2\2\u0117\u0115\3\2\2\2\u0117\u0116\3\2\2\2\u0118/\3\2\2\2"+
		"\u0119\u011a\5\62\32\2\u011a\u011b\7\27\2\2\u011b\u011c\7\13\2\2\u011c"+
		"\u011d\5\64\33\2\u011d\u011e\7\f\2\2\u011e\u011f\7\n\2\2\u011f\u0120\5"+
		"\66\34\2\u0120\u0121\7\t\2\2\u0121\u0122\58\35\2\u0122\61\3\2\2\2\u0123"+
		"\u0124\7\'\2\2\u0124\63\3\2\2\2\u0125\u0126\5@!\2\u0126\65\3\2\2\2\u0127"+
		"\u0128\5@!\2\u0128\67\3\2\2\2\u0129\u012a\5@!\2\u012a9\3\2\2\2\u012b\u012c"+
		"\7\r\2\2\u012c\u0131\7\'\2\2\u012d\u012e\7\b\2\2\u012e\u0130\7\'\2\2\u012f"+
		"\u012d\3\2\2\2\u0130\u0133\3\2\2\2\u0131\u012f\3\2\2\2\u0131\u0132\3\2"+
		"\2\2\u0132\u0134\3\2\2\2\u0133\u0131\3\2\2\2\u0134\u0135\7\16\2\2\u0135"+
		"\u0136\7\27\2\2\u0136\u0137\7\r\2\2\u0137\u013c\5@!\2\u0138\u0139\7\b"+
		"\2\2\u0139\u013b\5@!\2\u013a\u0138\3\2\2\2\u013b\u013e\3\2\2\2\u013c\u013a"+
		"\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u013f\3\2\2\2\u013e\u013c\3\2\2\2\u013f"+
		"\u0140\7\16\2\2\u0140;\3\2\2\2\u0141\u0142\5> \2\u0142\u014b\7\13\2\2"+
		"\u0143\u0148\5@!\2\u0144\u0145\7\b\2\2\u0145\u0147\5@!\2\u0146\u0144\3"+
		"\2\2\2\u0147\u014a\3\2\2\2\u0148\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149"+
		"\u014c\3\2\2\2\u014a\u0148\3\2\2\2\u014b\u0143\3\2\2\2\u014b\u014c\3\2"+
		"\2\2\u014c\u014d\3\2\2\2\u014d\u014e\7\f\2\2\u014e=\3\2\2\2\u014f\u0150"+
		"\7\'\2\2\u0150?\3\2\2\2\u0151\u0156\5B\"\2\u0152\u0153\7 \2\2\u0153\u0155"+
		"\5B\"\2\u0154\u0152\3\2\2\2\u0155\u0158\3\2\2\2\u0156\u0154\3\2\2\2\u0156"+
		"\u0157\3\2\2\2\u0157A\3\2\2\2\u0158\u0156\3\2\2\2\u0159\u015e\5D#\2\u015a"+
		"\u015b\7\37\2\2\u015b\u015d\5D#\2\u015c\u015a\3\2\2\2\u015d\u0160\3\2"+
		"\2\2\u015e\u015c\3\2\2\2\u015e\u015f\3\2\2\2\u015fC\3\2\2\2\u0160\u015e"+
		"\3\2\2\2\u0161\u0166\5F$\2\u0162\u0163\t\3\2\2\u0163\u0165\5F$\2\u0164"+
		"\u0162\3\2\2\2\u0165\u0168\3\2\2\2\u0166\u0164\3\2\2\2\u0166\u0167\3\2"+
		"\2\2\u0167E\3\2\2\2\u0168\u0166\3\2\2\2\u0169\u016b\t\4\2\2\u016a\u0169"+
		"\3\2\2\2\u016a\u016b\3\2\2\2\u016b\u016c\3\2\2\2\u016c\u0171\5H%\2\u016d"+
		"\u016e\t\4\2\2\u016e\u0170\5H%\2\u016f\u016d\3\2\2\2\u0170\u0173\3\2\2"+
		"\2\u0171\u016f\3\2\2\2\u0171\u0172\3\2\2\2\u0172G\3\2\2\2\u0173\u0171"+
		"\3\2\2\2\u0174\u0179\5J&\2\u0175\u0176\t\5\2\2\u0176\u0178\5J&\2\u0177"+
		"\u0175\3\2\2\2\u0178\u017b\3\2\2\2\u0179\u0177\3\2\2\2\u0179\u017a\3\2"+
		"\2\2\u017aI\3\2\2\2\u017b\u0179\3\2\2\2\u017c\u0187\7\'\2\2\u017d\u0187"+
		"\7%\2\2\u017e\u017f\7\13\2\2\u017f\u0180\5@!\2\u0180\u0181\7\f\2\2\u0181"+
		"\u0187\3\2\2\2\u0182\u0187\7&\2\2\u0183\u0187\5<\37\2\u0184\u0185\7\36"+
		"\2\2\u0185\u0187\5J&\2\u0186\u017c\3\2\2\2\u0186\u017d\3\2\2\2\u0186\u017e"+
		"\3\2\2\2\u0186\u0182\3\2\2\2\u0186\u0183\3\2\2\2\u0186\u0184\3\2\2\2\u0187"+
		"K\3\2\2\2\37OX^mu~\u0088\u008c\u0094\u00a3\u00a6\u00ac\u00bd\u00c3\u00d1"+
		"\u00db\u0111\u0117\u0131\u013c\u0148\u014b\u0156\u015e\u0166\u016a\u0171"+
		"\u0179\u0186";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}