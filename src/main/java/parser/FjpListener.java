package parser;

// Generated from ../Fjp.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link FjpParser}.
 */
public interface FjpListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link FjpParser#translation_unit}.
	 * @param ctx the parse tree
	 */
	void enterTranslation_unit(FjpParser.Translation_unitContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#translation_unit}.
	 * @param ctx the parse tree
	 */
	void exitTranslation_unit(FjpParser.Translation_unitContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(FjpParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(FjpParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#global_variables}.
	 * @param ctx the parse tree
	 */
	void enterGlobal_variables(FjpParser.Global_variablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#global_variables}.
	 * @param ctx the parse tree
	 */
	void exitGlobal_variables(FjpParser.Global_variablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(FjpParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(FjpParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(FjpParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(FjpParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#function_header}.
	 * @param ctx the parse tree
	 */
	void enterFunction_header(FjpParser.Function_headerContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#function_header}.
	 * @param ctx the parse tree
	 */
	void exitFunction_header(FjpParser.Function_headerContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#function_parameters}.
	 * @param ctx the parse tree
	 */
	void enterFunction_parameters(FjpParser.Function_parametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#function_parameters}.
	 * @param ctx the parse tree
	 */
	void exitFunction_parameters(FjpParser.Function_parametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#function_parameter}.
	 * @param ctx the parse tree
	 */
	void enterFunction_parameter(FjpParser.Function_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#function_parameter}.
	 * @param ctx the parse tree
	 */
	void exitFunction_parameter(FjpParser.Function_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#function_return}.
	 * @param ctx the parse tree
	 */
	void enterFunction_return(FjpParser.Function_returnContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#function_return}.
	 * @param ctx the parse tree
	 */
	void exitFunction_return(FjpParser.Function_returnContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#main_function}.
	 * @param ctx the parse tree
	 */
	void enterMain_function(FjpParser.Main_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#main_function}.
	 * @param ctx the parse tree
	 */
	void exitMain_function(FjpParser.Main_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#block_body}.
	 * @param ctx the parse tree
	 */
	void enterBlock_body(FjpParser.Block_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#block_body}.
	 * @param ctx the parse tree
	 */
	void exitBlock_body(FjpParser.Block_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#local_variables}.
	 * @param ctx the parse tree
	 */
	void enterLocal_variables(FjpParser.Local_variablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#local_variables}.
	 * @param ctx the parse tree
	 */
	void exitLocal_variables(FjpParser.Local_variablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCommand(FjpParser.CommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCommand(FjpParser.CommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#switch_command}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_command(FjpParser.Switch_commandContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#switch_command}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_command(FjpParser.Switch_commandContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#switch_condition}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_condition(FjpParser.Switch_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#switch_condition}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_condition(FjpParser.Switch_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#switch_case}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_case(FjpParser.Switch_caseContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#switch_case}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_case(FjpParser.Switch_caseContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#switch_case_expression}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_case_expression(FjpParser.Switch_case_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#switch_case_expression}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_case_expression(FjpParser.Switch_case_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#while_cycle}.
	 * @param ctx the parse tree
	 */
	void enterWhile_cycle(FjpParser.While_cycleContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#while_cycle}.
	 * @param ctx the parse tree
	 */
	void exitWhile_cycle(FjpParser.While_cycleContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#do_while}.
	 * @param ctx the parse tree
	 */
	void enterDo_while(FjpParser.Do_whileContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#do_while}.
	 * @param ctx the parse tree
	 */
	void exitDo_while(FjpParser.Do_whileContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#if_clause}.
	 * @param ctx the parse tree
	 */
	void enterIf_clause(FjpParser.If_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#if_clause}.
	 * @param ctx the parse tree
	 */
	void exitIf_clause(FjpParser.If_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#else_clause}.
	 * @param ctx the parse tree
	 */
	void enterElse_clause(FjpParser.Else_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#else_clause}.
	 * @param ctx the parse tree
	 */
	void exitElse_clause(FjpParser.Else_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#if_else_clause}.
	 * @param ctx the parse tree
	 */
	void enterIf_else_clause(FjpParser.If_else_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#if_else_clause}.
	 * @param ctx the parse tree
	 */
	void exitIf_else_clause(FjpParser.If_else_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(FjpParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(FjpParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#ternar_assignment}.
	 * @param ctx the parse tree
	 */
	void enterTernar_assignment(FjpParser.Ternar_assignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#ternar_assignment}.
	 * @param ctx the parse tree
	 */
	void exitTernar_assignment(FjpParser.Ternar_assignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#ternar_identifier}.
	 * @param ctx the parse tree
	 */
	void enterTernar_identifier(FjpParser.Ternar_identifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#ternar_identifier}.
	 * @param ctx the parse tree
	 */
	void exitTernar_identifier(FjpParser.Ternar_identifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#ternar_condition}.
	 * @param ctx the parse tree
	 */
	void enterTernar_condition(FjpParser.Ternar_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#ternar_condition}.
	 * @param ctx the parse tree
	 */
	void exitTernar_condition(FjpParser.Ternar_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#ternar_true}.
	 * @param ctx the parse tree
	 */
	void enterTernar_true(FjpParser.Ternar_trueContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#ternar_true}.
	 * @param ctx the parse tree
	 */
	void exitTernar_true(FjpParser.Ternar_trueContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#ternar_false}.
	 * @param ctx the parse tree
	 */
	void enterTernar_false(FjpParser.Ternar_falseContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#ternar_false}.
	 * @param ctx the parse tree
	 */
	void exitTernar_false(FjpParser.Ternar_falseContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#parallel_assignment}.
	 * @param ctx the parse tree
	 */
	void enterParallel_assignment(FjpParser.Parallel_assignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#parallel_assignment}.
	 * @param ctx the parse tree
	 */
	void exitParallel_assignment(FjpParser.Parallel_assignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#function_call}.
	 * @param ctx the parse tree
	 */
	void enterFunction_call(FjpParser.Function_callContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#function_call}.
	 * @param ctx the parse tree
	 */
	void exitFunction_call(FjpParser.Function_callContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#function_identifier}.
	 * @param ctx the parse tree
	 */
	void enterFunction_identifier(FjpParser.Function_identifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#function_identifier}.
	 * @param ctx the parse tree
	 */
	void exitFunction_identifier(FjpParser.Function_identifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(FjpParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(FjpParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#logical_term}.
	 * @param ctx the parse tree
	 */
	void enterLogical_term(FjpParser.Logical_termContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#logical_term}.
	 * @param ctx the parse tree
	 */
	void exitLogical_term(FjpParser.Logical_termContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#comparison}.
	 * @param ctx the parse tree
	 */
	void enterComparison(FjpParser.ComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#comparison}.
	 * @param ctx the parse tree
	 */
	void exitComparison(FjpParser.ComparisonContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#numeric_expression}.
	 * @param ctx the parse tree
	 */
	void enterNumeric_expression(FjpParser.Numeric_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#numeric_expression}.
	 * @param ctx the parse tree
	 */
	void exitNumeric_expression(FjpParser.Numeric_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#numeric_term}.
	 * @param ctx the parse tree
	 */
	void enterNumeric_term(FjpParser.Numeric_termContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#numeric_term}.
	 * @param ctx the parse tree
	 */
	void exitNumeric_term(FjpParser.Numeric_termContext ctx);
	/**
	 * Enter a parse tree produced by {@link FjpParser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactor(FjpParser.FactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link FjpParser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactor(FjpParser.FactorContext ctx);
}