grammar Fjp;

import LexerRules;

translation_unit:       (constant)*
                        global_variables
                        (variable SEMI)*
                        (function)*
                        main_function
                        ;

constant:               CONST IDENTIFIER ASSIGN NUMBER (COMMA IDENTIFIER ASSIGN NUMBER)* SEMI
                        ;

global_variables:       (variable)*
                        ;

variable:               INT IDENTIFIER (COMMA IDENTIFIER)* SEMI |
                        BOOL IDENTIFIER (COMMA IDENTIFIER)* SEMI
                        ;

function:               function_header LCURLY
                            block_body
                            (function_return SEMI)?
                        RCURLY
                        ;

function_header:        (INT|BOOL|VOID) IDENTIFIER LPAREN function_parameters RPAREN
                        ;

function_parameters:    (function_parameter (COMMA function_parameter)*)?
                        ;

function_parameter:     INT IDENTIFIER |
                        BOOL IDENTIFIER
                        ;
                        
function_return:        RETURN expression
                        ;

main_function:          VOID MAIN LPAREN RPAREN LCURLY
                            block_body
                        RCURLY
                        ;

block_body:             local_variables
                        (command)*
                        ;

local_variables:        (variable)*
                        ;

command:                assignment SEMI | 
                        function_call SEMI| 
                        if_clause |
                        if_else_clause |
                        while_cycle |
                        do_while |
                        switch_command
                        ;

switch_command:         SWITCH LPAREN switch_condition RPAREN LCURLY (switch_case)* RCURLY
                        ;

switch_condition:       expression
                        ;

switch_case:            CASE switch_case_expression COLON block_body
                        ;

switch_case_expression: expression
                        ;

while_cycle:            WHILE LPAREN expression RPAREN LCURLY block_body RCURLY
                        ;

do_while:               DO LCURLY block_body RCURLY WHILE LPAREN expression RPAREN SEMI
                        ;

if_clause:              IF LPAREN expression RPAREN LCURLY block_body RCURLY
                        ;

else_clause:            ELSE LCURLY block_body RCURLY
                        ;

if_else_clause:         if_clause else_clause
                        ;

assignment:             IDENTIFIER ASSIGN (IDENTIFIER ASSIGN)* expression |
                        ternar_assignment |
                        parallel_assignment 
                        ;

ternar_assignment:      ternar_identifier ASSIGN LPAREN ternar_condition RPAREN QM ternar_true COLON ternar_false
                        ;

ternar_identifier:      IDENTIFIER
                        ;

ternar_condition:       expression
                        ;

ternar_true:            expression
                        ;

ternar_false:           expression
                        ;

parallel_assignment:    LCURLY IDENTIFIER (COMMA IDENTIFIER)* RCURLY ASSIGN LCURLY expression (COMMA expression)* RCURLY
                        ;

function_call:          function_identifier LPAREN (expression (COMMA expression)*)? RPAREN
                        ;

function_identifier:    IDENTIFIER
                        ;

expression:             logical_term (OR logical_term)*
                        ;
                        
logical_term:           comparison (AND comparison)*
                        ;

comparison:             numeric_expression ((EQ|NEQ|LT|LE|GT|GE) numeric_expression)*
                        ;

numeric_expression:     (ADD|SUB)? numeric_term ((ADD|SUB) numeric_term)*
                        ;
                          
numeric_term:           factor ((MUL|DIV) factor)*
                        ;

factor:                 IDENTIFIER | NUMBER | LPAREN expression RPAREN | BOOL_VAL | function_call | NEG factor
                        ;          