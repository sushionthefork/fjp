grammar LexerRules;

CONST:      'const';
INT:        'int';
BOOL:       'bool';
VOID:       'void';

SEMI:       ';';
COMMA:      ',';
COLON:      ':';
QM:         '?';
LPAREN:     '(';
RPAREN:     ')';
LCURLY:     '{';
RCURLY:     '}';

MAIN:       'main';
RETURN:     'return';

IF:         'if';
ELSE:       'else';
WHILE:      'while';
DO:         'do';
SWITCH:     'switch';
CASE:       'case';

ASSIGN:     '=';
EQ:         '==';
NEQ:        '!=';
GT:         '>';
LT:         '<';
LE:         '<=';
GE:         '>=';
NEG:        '!';
AND:        '&&';
OR:         '||';
ADD:        '+';
SUB:        '-';
MUL:        '*';
DIV:        '/';

NUMBER:     ('+'|'-')?[0-9]+;
BOOL_VAL:   'true' | 'false' | NUMBER;
IDENTIFIER: [_a-zA-Z][_a-zA-Z0-9]*;

WS:         [ \t\r\n] -> skip;